#!/usr/bin/env python3
import fileinput
import re


for line in fileinput.input():
    search = re.search('DB update', line)
    if search is not None:
        search = re.search('Created calendar\s+([a-zA-Z0-9:-]+)\s+for user\s+([a-zA-Z0-9.@_-]+)\s+as\s+([a-zA-Z0-9:-]+)\s+', line)
    if search is not None:
        calId = search.group(1)
        email = search.group(2)
        uId = search.group(3)
        print(f'UPDATE "AO_950DC3_TC_SUBCALS" SET "CREATOR" = (SELECT user_key FROM user_mapping WHERE aaid=\'{uId}\') WHERE "ID" = \'{calId}\';')
        print(f'UPDATE "AO_950DC3_TC_SUBCALS" SET "CREATOR" = (SELECT user_key FROM user_mapping WHERE aaid=\'{uId}\') WHERE "PARENT_ID" = \'{calId}\';')
