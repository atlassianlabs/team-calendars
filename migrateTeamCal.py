#!/usr/bin/env python3
import argparse
from colorama import init, Fore, Back, Style
import csv
from dateutil.parser import *
from datetime import *
import inspect
import json
import os
import platform
import pytz
import re
import requests
from rich.console import Console
from rich.style import Style
import signal
from urllib.parse import urlencode, quote
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import urllib3

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

DEBUG = False
NOCOLOUR = False
DEVMODE = False
VERSION = "master"
outputFile = None
noOutput = False
QUOTES='"\' \t\n\r'

urllib3.disable_warnings()

headers = {
    'Content-type': 'application/json',
    'User-Agent': f'Team Calendar Migration Script {VERSION}'
}

stopOnMissingUser = False
stopOnFirstError = False

verboseFile = None

def get_request(**kwargs):
    response = None
    url = ''
    try:
        url = kwargs['url']
    except Exception as e:
        print_error('URL is mandatory for a GET request')
        return response
    try:
        response = requests.get(**kwargs)
#        print_debug('Response from {} = {}'.format(url, response.json()))
    except requests.exceptions.SSLError as SSLError:
        response = requests.get(verify=False, **kwargs)
    print_debug("Response from {} is {}".format(url, response))
    return response

def handler(signum, frame):
    print_warning("Ctrl-c was pressed. Exiting immediately.")
    exit(999)

signal.signal(signal.SIGINT, handler)

def host_os():
    return platform.system()

def show_version():
    print_info("Team calendar migration script version {}".format(VERSION), show_stack = False)

def print_verbose(message):
    global verboseFile
    if not verboseFile:
        verboseFile = open('devmode.out', "a")
    if isinstance(message, dict):
        msg = json.dumps(message)
    else:
        msg = message
    if DEVMODE:
        print(datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' [DEVMODE] ' + '[' + inspect.stack()[1].function + '] ' + msg, file=verboseFile)

def pretty_print_json(payload, msg=None):
    recurringEvent = []
    if type(payload) == list:
        for p in payload:
            if 'recur' in p:
                try:
                    evIDList = p['id'].split('/')
                    uniqId = "-".join(evIDList[1:])
                    if uniqId in recurringEvent:
                        continue
                    recurringEvent.append(uniqId)
                except ValueError as ve:
                    pass
            json_formatted_str = json.dumps(p, indent=4)
            print_verbose(msg + ":\n" + json_formatted_str)
    elif type(payload) == dict:
        json_formatted_str = json.dumps(payload, sort_keys=True, indent=4, separators=(',', ': '))
        print_verbose(msg + ":\n" + json_formatted_str)
    else:
        json_formatted_str = json.dumps(payload, indent=4)
        print_verbose(msg + ":\n" + json_formatted_str)

def print_debug(message):
    if isinstance(message, dict):
        msg = json.dumps(message)
    else:
        msg = message
    if DEBUG:
        callerFrameRecord = inspect.stack()[1]
        frame = callerFrameRecord[0]
        info = inspect.getframeinfo(frame)
        print_with_colour('at {}:{} {}'.format(info.function, info.lineno, msg), debug_style, "DEBUG")


def print_error(message):
    if isinstance(message, dict):
        msg = json.dumps(message)
    else:
        msg = message
    callerFrameRecord = inspect.stack()[1]
    frame = callerFrameRecord[0]
    info = inspect.getframeinfo(frame)
    print_with_colour('at {}:{} {}'.format(info.function, info.lineno, msg), error_style, "ERROR")
    if stopOnFirstError:
        exit(0)


def print_warning(message):
    if isinstance(message, dict):
        msg = json.dumps(message)
    else:
        msg = message
    callerFrameRecord = inspect.stack()[1]
    frame = callerFrameRecord[0]
    info = inspect.getframeinfo(frame)
    print_with_colour('at {}:{} {}'.format(info.function, info.lineno, msg), warning_style, "WARN")
    if stopOnFirstError:
        exit(0)

def print_with_colour(message, style=None, messageType="INFO"):
    asnow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    global reset_style
    if outputFile:
        print("{} {}".format(asnow, '[' + messageType + '] ' + message), file=outputFile)
    if not noOutput:
        console.print("{} {}".format(asnow, '[' + messageType + '] ' + message), style=style)
#        console.print("{} {}".format(asnow, '[' + str(messageType) + '] ' + str(message.encode(encoding = 'UTF-8'))), style=style)


def print_info(message, show_stack = True):
    if isinstance(message, dict):
        msg = json.dumps(message)
    else:
        msg = message
    if show_stack:
        callerFrameRecord = inspect.stack()[1]
        frame = callerFrameRecord[0]
        info = inspect.getframeinfo(frame)
        print_with_colour('at {}:{} {}'.format(info.function, info.lineno, msg), info_style, "INFO")
    else:
        print_with_colour(msg, info_style, "INFO")


def usage():
    if NOCOLOUR:
        style = ""
        reset = ""
    else:
        style = Style.BRIGHT + Fore.CYAN
        reset = Style.RESET_ALL
    usage_str = """migrateTeamCal.py -i <source_URL> -o <target_URL> -t <target_user> -k <target_password> -s <server_user> -p <server_password> -f <aid_file>
    where,
    <source_URL> is the URL of your Confluence Server instance (example: http://confluence.company.com:8090)
    <target_URL> is the URL of your Cloud instance (example: https://mycompany.atlassian.net)
    <target_user> is the email ID of the user connecting to the cloud instance
    <target_password> is the API Key for the user connecting to the cloud instance
    <server_user> is the username for Confluence Server instance
    <server_password> is the password for Confluence Server instance
    <aids_ file> is a CSV file mapping each user's email ID with their AID"""
    print(style + usage_str + reset)


def parse_arguments():
    global DEBUG
    global DEVMODE
    parser = argparse.ArgumentParser(prog='migrateTeamCal.py', description='Migrate Confluence Team Calendars')
    parser.add_argument('--source', '-s', type=str, required=True, help='Source Confluence Server Base URL (example: http://localhost:8090)')
    parser.add_argument('--target', '-t', type=str, required=True, help='Target Confluence Cloud URL (example: https://my-cloud-site.atlassian.net)')
    parser.add_argument('--source-user', '-su', type=str, required=True, help='Username for source')
    parser.add_argument('--source-password', '-sp', type=str, required=True, help='Password for source')
    parser.add_argument('--target-user', '-tu', type=str, required=True, help='Username (email id) for target')
    parser.add_argument('--target-password', '-tp', type=str, required=True, help='API Key of the user for target')
    parser.add_argument('--input-file', '-i', type=str, default="", help="Input file with space to calendar mapping")
    parser.add_argument('--aaid', '-a', type=str, required=True, help="A CSV formatted file containing mapping of each user's email ID with their respectie AID")
    parser.add_argument('--default-invitee', '-di', type=str, default="", help="Default invitee email (for events where all existing invitees may be invalid)")
    parser.add_argument('--default-creator', '-dc', type=str, default="", help="Default calendar creator email (for calendars where creator user is not found)")
    parser.add_argument('--space', '-k', type=str, help="Space key, if migrating a single space")
    parser.add_argument('--date-format', '-df', type=str, default="", help="Target Cloud date format")
    parser.add_argument('--time-format', '-tf', type=str, default="", help="Target Cloud time format")
    parser.add_argument('--stop-on-error', action='store_true', default=False, help="Force the script to stop on error")
    parser.add_argument('--logfile', '-l', type=str, default="", help="Logfile to write output to")
    parser.add_argument('--no-output', '-no', action='store_true', default=False, help="Turn off logging to screen/terminal (useful if running in background or via a cron job")
    parser.add_argument('--source-appid', '-sa', type=str, help='Jira Server Application ID')
    parser.add_argument('--target-appid', '-ta', type=str, help='Jira Cloud Application ID')
    parser.add_argument('--retain', '-r', action='store_true', default=True, help="Retain all migrated calendars in the migrator's view")
    parser.add_argument('--pagesize', '-ps', type=int, default=-1, help='Number of calendars to migrate (see also: --startat)')
    parser.add_argument('--startat', '-st', type=int, default=-1, help='Start number of calendars to migrate (see also: --pagesize)')
    parser.add_argument('--confirm', '-c', action='store_true', default=False, help='Confirm after migrating each calendar')
    parser.add_argument('--debug', '-d', action='store_true', default=False, help="Enable printing of debug messages")
    parser.add_argument('--verbose', '-v', action='store_true', default=False, help="Enable verbose messaging for developer reference")
    parser.add_argument('--preflight', '-p', action='store_true', default=False, help="Preflight checks mode")
    parser.add_argument('--email-map','-e', type=str, help="Mapping of server emails to cloud emails")
    parser.add_argument('--start-date', '-sd', type=str, default="", help='Only migrate events from this date ()')
    DEBUG = False
    DEVMODE = False
    args = parser.parse_args()
    v = vars(args)
    if v['pagesize'] * v['startat'] < 0:
        parser.error('\n\nERROR: Both --pagesize and --startat must be specified')
    if args.debug:
        DEBUG = True
    if args.verbose:
        DEVMODE = True
    return args

def getCloudSpaces(sink, user='admin', pword='admin'):
    global spaceList
    url = f'{makeSinkUrl(sink)}/api/v2/spaces'
    print_debug('Calling {} to fetch spaces from cloud {}'.format(url, sink))
    hasMore = True
    while hasMore:
        try:
            responsej = {}
            response = get_request(url=url, auth=(user, pword))
            if response.status_code == 200:
                responsej = response.json()
                spaces = responsej['results']
                for space in spaces:
                    if space['key'] == '1ACEM':
                        print_debug("Found space 1ACEM!")
                    if space['key'] not in spaceList:
                        spaceList.append(space['key'])
                print_verbose(','.join(spaceList))
            if '_links' in responsej:
                if 'next' in responsej['_links']:
                    if responsej['_links']['next'] != None:
                        url = sink + responsej['_links']['next']
                        continue
                hasMore = False
        except Exception as e:
            print_error("Could not fetch migrated spaces from {} due to error:\n{}".format(sink, e))
            response.status_code = 0
    return response.status_code

def validateUrl(url):
    TIMEOUT=10
    try:
        response = get_request(url=url, timeout=TIMEOUT)
        print_debug('Status code: {}, response JSON from {}: {}'.format(response.status_code, url, response))
        if response.status_code == 404:
            raise requests.exceptions.ConnectionError
    except requests.exceptions.HTTPError as he:
        print_error('HTTP Error occurred when validating URL {}:\n{}'.format(url,he))
        return False
    except requests.exceptions.ConnectionError as ce:
        print_error('Please validate if URL {} is correct and re-run the script.'.format(url))
        print_verbose('Error when validating URL {}:\n{}'.format(url, ce))
        return False
    except requests.exceptions.Timeout as toe:
        print_error('Requst timed out trying to reach {} within {} seconds.'.format(url,TIMEOUT))
        print_verbose('Timeout ({} seconds) error for URL {}:\n{}'.format(TIMEOUT, url, toe))
        return False
    except requests.exceptions.RequestException as e:
    # catastrophic error. bail.
        raise SystemExit(e)
    try:
        response = get_request(url=url, timeout=TIMEOUT)
        if response.status_code == 200:
            return True
#            if not 'atlassian.net' in url:
#                try:
#                    print_verbose('Response from {} = {}'.format(url, response))
#                    rjson = response.json()
#                    print_verbose('Response JSON from {}: {}'.format(url, rjson))
#                    if rjson['state'] == 'RUNNING':
#                        return True
#                except Exception as e:
#                    print_debug('Could not convert response to JSON due to error {}'.format(e))
#                    return False
#            else:
#                return True
        elif response.status_code == 302:
            return True
        else:
            print_error('The Confluence server at {} is not running or not available. Please fix and retry.'.format(url))
            print_verbose('Error fetching status for {}:\n{}'.format(url, response))
            return False
    except urllib3.exceptions.InsecureRequestWarning as irw:
        pass
    except Exception as e:
        print_error('Unknown error occurred trying to check status of {}:\nStatus Code: {}\nException: {}'.format(url,response.status_code, e))
        return False

def makeSourceUrl(source):
    return noslash(source)


def makeSinkUrl(sink):
    if re.search(r"\b" + 'wiki' + r"\b", sink):
        return noslash(sink)
    newsink = noslash(sink) + '/wiki'
    return newsink


def getCalendars(source, user='admin', pword='admin'):
    response = None
    r = f'{makeSourceUrl(source)}/rest/calendar-services/1.0/calendar/subcalendars'
    print_verbose("{}".format(r))
    try:
        response = get_request(url=r, auth=(user, pword)).json()
    except urllib3.exceptions.InsecureRequestWarning as irw:
        pass
    return response['payload']


def getAllCalendars(source, start, limit, user='admin', pword='admin', space=''):
    params = ''
    if space:
        params = f'&space={space}'
    r = f'{makeSourceUrl(source)}/rest/calendar-services/1.0/calendar/search/subcalendars/bulk?startIndex={start}&pageSize={limit}{params}'
    print_verbose("{}".format(r))
    response = None
    try:
        response = get_request(url=r, auth=(user, pword)).json()
        print_verbose("In getAllCalendars, response = {}".format(response))
        return response['payload']
    except KeyError:
        print_debug(f"Verify if any calendar(s) exist in the space (if provided)")
        return None
    except urllib3.exceptions.InsecureRequestWarning as irw:
        pass
    except Exception as Err:
        print_error(f"Check source Confluence site '{source}' if:\n1. Source server is reachable\n2. Team Calendars plugin is installed\n3. License for the plugin is current\n4. Username/Password is correct")
        print_verbose(("Error when fetching calendars from {}:\n{}".format(source,Err)))
        raise

def getSomeCalendars(source, inc, user='admin', pword='admin'):
    url = f'{makeSourceUrl(source)}/rest/calendar-services/1.0/calendar/subcalendars'
    inclood = '?'
    for i in inc:
        inclood = inclood + f'include={i}&'
    print_verbose("{}".format(url + inclood[:-1]))
    response = None
    try:
        response = get_request(url=url + inclood[:-1], auth=(user, pword)).json()
        print_verbose("Response from {}: {}".format(source, response))
        return response['payload']
    except KeyError:
        print_debug(f"Verify if the following calendar(s) exist by browsing to {makeSourceUrl(source)}/display/<space key>/calendar/<calendar id>\n{inc}")
        return None
    except Exception as Err:
        print_error(f"Check source Confluence site '{source}' if:\n1. Source server is reachable\n2. Team Calendars plugin is installed\n3. License for the plugin is current\n4. Username/Password is correct")
        print_verbose(("Error when fetching calendars from {}:\n{}".format(source,Err)))
        return None

def getSomeCalendarsSink(sink, inc, user='admin', pword='admin'):
    url = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/subcalendars'
    inclood = '?'
    for i in inc:
        inclood = inclood + f'include={i}&'
    print_verbose("{}".format(url + inclood[:-1]))
    response = None
    try:
        response = get_request(url=url + inclood[:-1], auth=(user, pword)).json()
        print_verbose("Response from {}: {}".format(sink, response))
        return response['payload']
    except KeyError:
#        print_debug(f"Verify if the following calendar(s) exist by browsing to {makeSinkUrl(sink)}/display/<space key>/calendar/<calendar id>\n{inc}")
        return None
    except Exception as Err:
        print_error(f"Check target Confluence site '{sink}' if:\n1. Target server is reachable\n2. Team Calendars plugin is installed\n3. License for the plugin is current\n4. Username/Password is correct")
        print_verbose(("Error when fetching calendars from {}:\n{}".format(sink,Err)))
        raise


def getCalendarUsers(source, start=0, limit=100, user='admin', pword='admin'):
    url = f'{makeSourceUrl(source)}/rest/calendar-services/1.0/calendar/preferences/bulk?start={start}&limit={limit}'
    print_verbose("{}".format(url))
    response = None
    response_json = None
    try:
        response = get_request(url=url, auth=(user, pword))
        print_verbose("Response = {}".format(response))
        if response.status_code == 200:
            try:
                response_json = response.json()
                print_verbose("Response (JSON) = {}".format(response_json))
            except Exception as json_err:
                print_debug('Could not obtain JSON payload due to error {}'.format(json_err))
                response_json = None
        else:
            print_debug('Response code when fetching calendar users: {}'.format(response.status_code))
            response_json = None
    except Exception as Err:
        print_debug("Failed to fetch calendar users from {} due to error:\n{}".format(url, Err))
        response_json = None
    return response_json

def getUserPermissions(uid, source, user='admin', pword='admin'):
    url = f'{makeSourceUrl(source)}/rest/calendar-services/1.0/calendar/preferences?user={uid}'
    print_verbose("{}".format(url))
    response = None
    try:
        response = get_request(url=url, auth=(user, pword)).json()
        print_verbose("In getUserPermissions, response = {}".format(response))
    except Exception as Err:
        print_debug("Could not get permissions/restrictions for user {}. The user may not exist on server.".format(uid))    
        response = None
    return response


def putPermissionView(uid, view, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    form = {
        'view': view
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/preferences/view?accountid={uid}'
    print_verbose("Executing endpoint {} with form data {}".format(r,form))
    response = None
    try:
        response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form)
        print_verbose("Response from endpoint '{}' with form data: '{}' is {}".format(r, form, response))
        if response.ok:
            print_info("Created a permission '{}' for user '{}'".format(form['view'], uid))
    except Exception as putPVErr:
        print_error("Could not create permission '{}' for user '{}'".format(form['view'], uid))
        print_verbose("Error when creating permission '{}' for user '{}': {}".format(form['view'], uid, putPVErr))
    return response.status_code == 200


def putWatch(uid, calId, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    form = {
        'subCalendarId': calId
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/preferences/subcalendars/watch?accountid={uid}'
    print_verbose("Executing endpoint {} with form data {}".format(r,form))
    response = None
    try:
        response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form)
        print_verbose("Response from endpoint '{}' with form data: '{}' is {}".format(r, form, response))
        if response.ok:
            print_info("Created a watch on '{}' for user '{}'".format(form['subCalendarId'], uid))
    except Exception as putPVErr:
        print_error("Could not create watch on '{}' for user '{}'".format(form['subCalendarId'], uid))
        print_verbose("Error when creating watch on '{}' for user '{}': {}".format(form['subCalendarId'], uid, putPVErr))
    return response.status_code == 200


def putDisableKey(uid, key, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    form = {
        'messageKey': key
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/preferences/messagekey?accountid={uid}'
    print_verbose("Executing endpoint {} with json data {}".format(r,form))
    response = None
    try:
        response = requests.delete(r, auth=(uname, upass), headers=myheaders, data=form)
        print_verbose("Response from endpoint '{}' with form data: '{}' is {}".format(r, form, response))
        if response.ok:
            print_info("Created a disable key '{}' for user '{}'".format(form['messageKey'], uid))
    except Exception as putPVErr:
        print_error("Could not create disable key '{}' for user '{}'".format(form['messageKey'], uid))
        print_verbose("Error when creating disable key '{}' for user '{}': {}".format(form['messageKey'], uid, putPVErr))
    return response.status_code == 200


def putInView(uid, calId, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    form = {
        'watchSubCalendars': 'true',
        'subCalendarIds': calId
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/subcalendars/subscribe?accountid={uid}'
    print_verbose("Executing endpoint {} with json data {}".format(r,form))
    response = None
    try:
        response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form)
        print_verbose("Response from endpoint '{}' with form data: '{}' is {}".format(r, form, response))
        if response.ok:
            print_info("Created a view on '{}' for user '{}'".format(form['subCalendarIds'], uid))
    except Exception as putPVErr:
        print_error("Could not create view on '{}' for user '{}'".format(form['subCalendarIds'], uid))
        print_verbose("Error when creating view on '{}' for user '{}': {}".format(form['subCalendarIds'], uid, putPVErr))
    return response.status_code == 200


def putCalendar(form, uid, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
        'User-Agent': f'Team Calendar Migration Script {VERSION}'
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/subcalendars?accountid={uid}'
    print_verbose("Calling endpoint '{}' with form data: '{}'".format(r, form))
    response = None
    try:
        response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form).json()
        print_verbose("Response from endpoint '{}' with form data: '{}' is {}".format(r, form, response))
        if 'success' in response:
            print_info("Created a calendar named '{}' (New ID: {}) for user '{}'".format(form['name'], response['modifiedSubCalendarId'], form['creator']))
            if 'payload' in response:
                print_verbose("Payload for new calendar ID '{}': {}".format(response['modifiedSubCalendarId'],response['payload']))
            return response['modifiedSubCalendarId']
    except Exception as putCalErr:
        print_error("Could not create calendar '{}' in space '{}'".format(form['name'], form['spaceKey']))
        print_verbose("Error when creating calendar '{}' in space '{}': {}".format(form['name'],form['spaceKey'], putCalErr))
        return None


def putRestrict(form, uid, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
        'User-Agent': f'Team Calendar Migration Script {VERSION}'
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/subcalendars/restrictions?accountid={uid}'
    print_verbose("Calling endpoint '{}' with form data: '{}'".format(r, form))
    response = None
    try:
        response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form).json()
        print_verbose("Response from endpoint '{}' with form data: '{}' is {}".format(r, form, response))
        if 'success' in response:
            print_info("Restricted a calendar id '{}' for user '{}'".format(form['subCalendarId'], uid))
            print_verbose("Payload for calendar ID '{}': {}".format(form['subCalendarId'], response['payload']))
            return response['payload']
    except Exception as putCalErr:
        print_error("Could not restrict calendar '{}' in space '{}'".format(form['name'], form['spaceKey']))
        print_verbose("Error when restricting calendar '{}' in space '{}': {}".format(form['name'],form['spaceKey'], putCalErr))
        return None


def hideCalendar(form, sink, uname, upass):
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    response = None
    try:
        response = requests.delete(f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/subcalendars/preferences', auth=(uname, upass), headers=myheaders, data=form).json()
        print_info("Hiding calendar id '{}'".format(form['subCalendarId']))
        print_verbose(("Response for hiding calendar '{}': {}".format(form['subCalendarId'], response)))
        return response['success']
    except Exception as hideError:
        print_error(("Error occurred when hiding calendar '{}': {}".format(form['subCalendarId'], hideError)))
        return None

def countEvents(events=[]):
    count = 0
    recurArray = []
    for event in events:
        if 'recur' in event:
            try:
                evIDList = event['id'].split('/')
                uniqId = "-".join(evIDList[1:])
                if uniqId in recurArray:
                    continue
                else:
                    recurArray.append(uniqId)
                    count += 1
            except ValueError as ve:
                print_debug("Invalid Event ID: {}".format(event['id']))
                continue
        else:
            count += 1
    return count

def fix_date(time_str=None):
    if not time_str:
        return "2000-01-01T00:00:00Z"
    if re.search(r"^\\d{4}-\\d{2}-\\d{2}", time_str) is not None:
        if re.search("T\\d{2}:\\d{2}:\\d{2}Z", time_str) is not None:
            print_verbose("Start Date: {}".format(time_str))
            return time_str
        else:
            print_verbose("Start Date: {}T00:00:00Z".format(time_str[:10]))
            return time_str[:10] + "T00:00:00Z"
    elif re.search(r"^\\d{2}-\\d{2}-\\d{2}", time_str) is not None:
        print_verbose("Start Date: 20{}T00:00:00Z".format(time_str))
        return "20" + time_str + "T00:00:00Z"
    print_debug("Invalid start time ({}), using default".format(time_str))
    return "2000-01-01T00:00:00Z"

def getEvents(cal, source, user='admin', pword='admin', start_date=f'2000-01-01T00:00:00Z'):
    start = 'start={}'.format(fix_date(start_date))
    end = f'end=2030-12-31T00:00:00Z'
    response = None
    try:
        response = get_request(url=f'{makeSourceUrl(source)}/rest/calendar-services/1.0/calendar/events?subCalendarId={cal}&{start}&{end}', auth=(user, pword)).json()
        print_verbose("Response when fetching events for calendar '{}': {}".format(cal, response))
        if 'events' in response:
            pretty_print_json(response['events'], 'Events fetched from server')
            return response['events']
        return []
    except ValueError as ve:
        return response['events']
    except Exception as err:
        print_error(("Could not fetch events for calendar '{}' from '{}' due to error:\n{}\nResponse (if any):\n{}".format(cal, source, err, response)))
        return None

defaultInvitee = ""

def putEvent(form, owner, sink, uname, upass, aaid=""):
    global defaultInvitee
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/events.json?accountId={owner}'
    print_verbose("Endpoint: {}, Form Data: {}".format(r, form))
    response = None
    try:
        response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form).json()
        if 'success' in response:
            if not response['success']:
                if 'fieldErrors' in response:
                    fieldErrors = response['fieldErrors']
                    for fError in fieldErrors:
                        if fError['field'] == 'who' and fError['errorMessages'][0] == 'Please select a valid user':
                            print_warning("Failed to create an event named '{}' due to missing invitee list.".format(form['what']))
                            if defaultInvitee and len(defaultInvitee) > 0:
                                defaultInviteeId = lookupFile(defaultInvitee, aaid)
                                form['person'] = 'ari:cloud:identity::user/' + defaultInviteeId
                                r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/events.json'
                                print_verbose("Endpoint: {}, Form Data: {}".format(r, form))
                                try:
                                    response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form).json()
                                    if 'success' in response:
                                        if not response['success']:
                                            print_error("Creating event '{}' with default invitee '{}' failed.".format(form['what'], defaultInvitee))
                                        else:
                                            print_info("Event '{}' created with default invitee '{}' starting on '{}'".format(form['what'], defaultInvitee, form['startDate']))
                                except Exception as eventError:
                                    print_error("Creating event '{}' with default invitee '{}' failed.".format(form['what'], defaultInvitee))
                                    print_debug("Creating event '{}' with default invitee '{}' failed. Error: {}".format(form['what'], defaultInvitee, eventError))
            else:
                print_info("Created an event named '{}' in calendar '{}' starting on '{}'".format(form['what'], form['subCalendarId'], form['startDate']))
            return response['success']
        else:
            print_error('Error creating event "{}". Events such as "Jira Project Releases", "Jira Agile Sprints", and "Jira Issue Dates" are not migrated by this script'.format(form['what']))
            print_verbose('Jira project-related event "{}" could not be migrated. Error stack:\n{}'.format(form['what'], response[0]))
            return False
    except Exception as putEventErr:
        print_error("Could not create event '{}' due to error: {}".format(form['what'], putEventErr))
        return False

def writeICS(eventForm=None):
    # Library to create ical for a single event: https://www.unadulterated-faff.com/articles/2020/02/11/creating-ics-files-in-python.html
    # Read/Write .ics files: https://stackoverflow.com/questions/3408097/parsing-files-ics-icalendar-using-python
    if not eventForm:
        return


def makeCal(blob, creator):
    try:
        asnow = datetime.utcnow().isoformat().split('.')[0]
        print_debug("Calendar creator: {}".format(creator))
        dict = {
            'subCalendarId': blob.get('subCalendarId', ''),
            'creator': creator,
            'type': blob.get('type', ''),
            'name': blob.get('name', ''),
            'description': blob.get('description', ''),
            'color': blob.get('color', ''),
            'spaceKey': blob.get('spaceKey', ''),
            'timeZoneId': blob.get('timeZoneId', ''),
            'location': blob.get('location', ''),
        }
        if len(blob.get('customEventTypes', '')) > 0:
            dict['customEventTypes'] = blob['customEventTypes']
        print_verbose("Dict: {}".format(dict))
        return dict
    except Exception as makeCalError:
        print_debug("Error occurred creating calendar payload: {}".format(makeCalError))
        return None


def makeRestrict(blob, subBlob, id, subcals, uid, source, s_user, s_pword, sink, t_user, t_pword, aaid=""):
    dict = {
        'subCalendarId': id,
        'spaceKey': subBlob.get('spaceKey', ''),
        'include': subcals
    }
    # only add invitees if the source event has them. For example, public holidays won't have any invitee.
    whos = getIds(getUserEmailList(getNames(blob.get('usersPermittedToView', '')), source, s_user, s_pword), sink, t_user, t_pword, uid, aaid)
    if whos and len(whos) > 0:
        dict['usersPermittedToView'] = whos
        dict['updateUsersPermittedToView'] = 'true'
    else:
        dict['updateUsersPermittedToView'] = 'false'
    whos = getIds(getUserEmailList(getNames(blob.get('usersPermittedToEdit', '')), source, s_user, s_pword), sink, t_user, t_pword, uid, aaid)
    if whos and len(whos) > 0:
        dict['usersPermittedToEdit'] = whos
        dict['updateUsersPermittedToEdit'] = 'true'
    else:
        dict['updateUsersPermittedToEdit'] = 'false'
    grps = blob.get('groupsPermittedToView')
    if grps and len(grps) > 0:
        dict['groupsPermittedToView'] = grps
        dict['updateGroupsPermittedToView'] = 'true'
    else:
        dict['updateGroupsPermittedToView'] = 'false'
    grps = blob.get('groupsPermittedToEdit')
    if grps and len(grps) > 0:
        dict['groupsPermittedToEdit'] = grps
        dict['updateGroupsPermittedToEdit'] = 'true'
    else:
        dict['updateGroupsPermittedToEdit'] = 'false'
    print_verbose("Dict: {}".format(dict))
    return dict

def makeHide(id):
    dict = {
        'subCalendarId': id,
        'calendarContext': 'myCalendars'
    }
    return dict


def makeSubscribe(blob, creator, id):
    dict = {
        'subCalendarId': id,
        'parentId': id,
        'creator': creator,
        'type': 'subscription',
        'name': blob.get('name', ''),
        'description': blob.get('description', ''),
        'color': blob.get('color', ''),
        'spaceKey': blob.get('spaceKey', ''),
        'timeZoneId': blob.get('timeZoneId', ''),
        'location': blob.get('location', ''),
    }
    return dict


timeFormat = "%H:%M"

def makeTime(time, sink, user, pword):
    global timeFormat
    try:
        # Get the date format from the target server
        # Use the URL {makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/util/format/date.json?date=130219701700&date=130219701730&pattern=time
        t = datetime.strptime(time, "%H:%M %p").strftime("31011970%H%M")
        r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/util/format/date.json?date={t}&date={t}&pattern=time'
        print_verbose("Executing {}".format(r))
        response = None
        try:
            response = get_request(url=r, auth=(user, pword))
            return response.json()[0]
        except Exception as makeTimeException:
            print_warning("Could not fetch time format. Error: {}".format(makeTimeException))
            return datetime.strptime(time, "%H:%M %p").strftime(timeFormat)
    except:
        # Get the date format from the target server
        # Use the URL {makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/util/format/date.json?date=130219701700&date=130219701730&pattern=time
        t = datetime.strptime(time, "%H:%M").strftime("31011970%H%M")
        r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/util/format/date.json?date={t}&date={t}&pattern=time'
        print_verbose("Executing {}".format(r))
        response = None
        try:
            response = get_request(url=r, auth=(user, pword))
            return response.json()[0]
        except Exception as makeTimeException:
            print_warning("Could not fetch time format. Error: {}".format(makeTimeException))
            return datetime.strptime(time, "%H:%M").strftime(timeFormat)


dateFormat = "%d %b, %Y"

def makeDate(date, sink, user, pword):
    d = parse(date)
    ds = d.strftime("%d%m%Y0000")
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/util/format/date.json?date={ds}&date={ds}&pattern=date'
    print_verbose("Executing {}".format(r))
    response = None
    try:
        response = get_request(url=r, auth=(user, pword))
        print_verbose("Converted date: {}".format(response.json()[0]))
        return response.json()[0]
    except Exception as makeDateException:
        print_warning("Could not fetch date format. Error: {}".format(makeDateException))
        raise

def makeEvent(id, blob, uid, sink, user, pword, oappid, nappid, aaid=""):
    global customTypeStore
    print_verbose("Form received: {}".format(blob))
    serverTZ = datetime.fromisoformat(blob.get('originalStartDateTime','')).tzname()
    try:
        if serverTZ == 'UTC':
            sd = datetime.fromisoformat(blob.get('originalStartDateTime','')).strftime('%b %d, %Y')
            ed = datetime.fromisoformat(blob.get('originalEndDateTime','')).strftime('%b %d, %Y')
        else:
            sd = datetime.fromisoformat(blob.get('originalStartDateTime','')).astimezone(pytz.timezone('Etc/UTC')).strftime('%b %d, %Y')
            ed = datetime.fromisoformat(blob.get('originalEndDateTime','')).astimezone(pytz.timezone('Etc/UTC')).strftime('%b %d, %Y')
    except Exception as makeDateException:
        print_error("Exception occurred when converting dates for event '{}': {}".format(blob, makeDateException))
        return None
    eventDict = {
        'originalSubCalendarId': id,
        'subCalendarId': id,
        'originalEventType': blob.get('eventType', ''),
        'eventType': blob.get('eventType', ''),
        'originalStartDate': blob.get('originalStartDate', ''),
        'what': contentMap(blob.get('title', ''), oappid, nappid),
        'url': contentMap(blob.get('url', ''), oappid, nappid),
        'isDragAndDropUpdate': 'false',
        'startDate': sd,
        'endDate': ed,
        'where': blob.get('location', ''),
        'description': contentMap(blob.get('description', ''), oappid, nappid),
        'recurrenceId': blob.get('recurrenceId', ''),
        'rruleStr': blob.get('rruleStr', ''),
        'isEditAllInRecurrenceSeries': 'true',
        'allDayEvent': blob.get('allDay', 'false'),
        # send startTime and endTime only if not allDayEvent
        # send userTimeZoneId as fetched from the server (if fetched at all)
    }
    print_verbose("Event field: allDay, type: {}, value: {}".format(type(blob.get('allDay')), blob.get('allDay')))
    if not blob.get('allDay', False):
        try:
            if serverTZ == 'UTC':
                st = datetime.fromisoformat(blob.get('originalStartDateTime','')).strftime('%-I:%M %p')
                et = datetime.fromisoformat(blob.get('originalEndDateTime','')).strftime('%-I:%M %p')
            else:
                st = datetime.fromisoformat(blob.get('originalStartDateTime','')).astimezone(pytz.timezone('Etc/UTC')).strftime('%-I:%M %p')
                et = datetime.fromisoformat(blob.get('originalEndDateTime','')).astimezone(pytz.timezone('Etc/UTC')).strftime('%-I:%M %p')
        except Exception as makeTimeException:
            print_error("Exception occurred when converting times: '{}'".format(makeTimeException))
            return None
        eventDict['startTime'] = st
        eventDict['endTime'] = et
    
    # only add invitees if the source event has them. For example, public holidays won't have any invitee.
    whos = getIds(getEmails(blob.get('invitees', '')), sink, user, pword, uid, aaid)
    if whos and len(whos) > 0:
            eventDict['person'] = whos
    # we have no way of dealing with custom event types - they're just 'other'
    if len(blob.get('uid', '')) > 0:
        eventDict['uid'] = blob['uid']
    if len(blob.get('recur', '')) > 0:
        eventDict['freq'] = blob.get('recur').get('freq', '')
        eventDict['byDay'] = blob.get('recur').get('byDay', '')
        eventDict['interval'] = blob.get('recur').get('interval', '')
        eventDict['isRepeatEnds'] = len(blob.get('recur').get('until', '')) > 0
        eventDict['until'] = blob.get('recur').get('until', '')
    try:
        if len(blob.get('customEventTypeId', '')) > 0:
            print_debug('Fetching new custom event type id for {} from global customTypeStore dictionary:\n{}'.format(blob['customEventTypeId'],customTypeStore))
            eventDict['customEventTypeId'] = customTypeStore[blob['customEventTypeId']]
            eventDict['originalCustomEventTypeId'] = customTypeStore[blob['customEventTypeId']]
    except Exception as e:
        print_error('Error when adding custom event type to event dictionary: custom type id {} not found!'.format(e))
        return None
    print_verbose('Event payload: {}'.format(eventDict))
    pretty_print_json(eventDict, 'Event payload for Cloud')
    return eventDict


def contentMap(content, fromapp, toapp):
    if fromapp and fromapp in content:
        return content.replace(fromapp, toapp)
    return content


def getPeople(blob, uid):
    if len(uid) > 0:
        return uid
    if len(blob) == 0:
        return ''
    string = ''
    for i in blob:
        string = string + ',' + i.get('id', '')
    return string[1:]


def getEmails(blob):
    if len(blob) == 0:
        return []
    emails = []
    dnames = []
    for i in blob:
        emails.append(i.get('email', ''))
        dnames.append(i.get('displayName', ''))
    return emails, dnames

def getNames(blob):
    if len(blob) == 0:
        return []
    names = []
    for i in blob:
        names.append(i.get('name', ''))
    return names

def getIds(ednames, sink, user, pword, defUid, aaid="", useAri=True):
    if not ednames or len(ednames[0]) == 0:
        return ''
    invitees = []
    string = ','
    index = 0
    while index < len(ednames[0]):
        # this event has the source username and useremail
        username = ednames[1][index]
        useremail = ednames[0][index]
        useremail = useremail.lower()
        if len(aaid) > 0:
            uid = lookupFile(useremail, aaid)
        else:
            uid = getUser(sink, username, useremail, user, pword)
        if len(uid) > 0:
            user_ = ''
            if useAri:
                user_ = 'ari:cloud:identity::user/'
            invitees.append(user_ + uid)
        else:
            print_warning(f"Could not validate user '{username}'. Removing from invitee list.")
        index += 1
    response = None
    for invitee in invitees:
        # Validate all invitees of the event
        inviteeId = invitee
        if useAri:
            inviteeId = invitee.split('/')[1]
        r = f'{makeSinkUrl(sink)}/rest/api/user?accountId={inviteeId}'
        raw = get_request(url=r, auth=(user, pword))
        response = raw.json()
        if 'displayName' in response:
            name = response['displayName']
            try:
                encoded_name = quote('"'+name+'"')
                params = "cql=user.fullname~{}".format(encoded_name)
                url = f'{makeSinkUrl(sink)}/rest/api/search/user?{params}'
                print_verbose("Calling {} to search user on cloud".format(url))
                raw = get_request(url=url, auth=(user, pword))
                response = raw.json()
            except Exception as e:
                print_warning(f"Could not validate user '{name}'. Removing from invitee list.")
                invitees.remove(invitee)
    return invitees


customTypeStore = {}


def putCustomType(form, sink, uname, upass, oldType):
    global customTypeStore
    myheaders = {
        'Content-type': 'application/x-www-form-urlencoded',
    }
    r = f'{makeSinkUrl(sink)}/rest/calendar-services/1.0/calendar/eventtype/custom'
    print_verbose("Executing endpoint {} with form data {}".format(r,form))
    response = None
    response = requests.put(r, auth=(uname, upass), headers=myheaders, data=form)
    print_verbose("Response from {}:\n{}".format(sink, response.json()))
    result = response.json()
    if 'customEventTypeId' in result:
        newType = result['customEventTypeId']
        customTypeStore[oldType] = newType
        if 'title' in form:
            print_info("New Custom Type '{}' created.".format(form['title']))
        return result
    else:
        print_warning("Could not create Custom Event Type '{}'!".format(form['title']))


def makeType(id, type):
    return {
        'subCalendarId': id,
        'title': type['title'],
        'icon': type['icon'],
        'periodInMins': type['periodInMins']
    }


def timefix(string):
    parts = string.split()
    if len(parts) > 1:
        if "pm" in parts[1].lower():
            colons = string.split(":")
            hours = int(colons[0])
            if int(colons[0]) < 12:
                hours = int(colons[0]) + 12
            return str(hours) + ":" + colons[1]
    return parts[0]


sourceNameStore = {}

def getUserName(source, uid, user, pword):
    response = None
    if uid not in sourceNameStore:
        params = urlencode({'key': uid})
        r = f'{makeSourceUrl(source)}/rest/api/user?{params}'
        print_verbose('Calling endpoint {}'.format(r))
        try:
            response = get_request(url=r, auth=(user, pword)).json()
            if 'username' in response:
                sourceNameStore[uid] = response['username']
            else:
                print_warning('No username found in server response for user {}'.format(uid))
                return None
        except Exception as e:
            print_error('Error fetching username for uid {} from {}:\n{}'.format(uid, source, e))
            return None
    return sourceNameStore[uid]

cloudUsers = {}

def getMappedEmail(email, email_map):
    global emailMapStore
    searchEmail = email
    if searchEmail in emailMapStore:
        return emailMapStore[searchEmail]
    if email_map and len(email_map) > 0:
        with open(email_map) as emf:
            for num, line in enumerate(emf):
                bits = line.split(',')
                search = re.search('<([^>]+)>', bits[0], re.IGNORECASE)
                if search is None:
                    search = re.search('([a-zA-Z0-9_.-]+[@][a-zA-Z0-9_.-]+)', bits[0], re.IGNORECASE)
                if search is not None:
                    serverEmail = search.group(1)
                    cloudEmail = bits[1].strip()
                    if serverEmail not in emailMapStore:
                        emailMapStore[serverEmail] = cloudEmail
                    if serverEmail == searchEmail:
                        return cloudEmail

def validateCloudUser(source, target, uid, suser, spword, tuser, tpword, aaid='', email_map=None, default_creator=None):
    global cloudUsers
    uname = getUserName(source, uid, suser, spword)
    if not uname:
        if not default_creator:
            print_error("Could not fetch username for uid {} from {} (try using --default-creator parameter)".format(uid, source))
            return None
    else:
        print_debug("Username for uid {} on {} is {}".format(uid, source, uname))
    response = getUserEmail(source, uname, suser, spword)
    if not response:
        if not default_creator:
            print_error("Could not fetch email for uid {} from {} (try using --default-creator parameter)".format(uid, source))
            return None
        else:
            umail = default_creator
            print_warning("Invalid user {}. Using default creator parameter, {}, instead.".format(uid, default_creator))
    else:
        umail = response[0].lower()
        if uid not in cloudUsers:
            cloudUsers[uid] = {"uname": uname, "email": umail}
        print_debug("Email for uid {} on {} is {}".format(uid, source, umail))
    cloudEmail = getMappedEmail(umail, email_map)
    cloudId = lookupFile(umail, aaid, email_map)
    if len(cloudId) == 0:
        print_error("Could not find AAID (Atlassian ID) for user: [{}, {}, {}] on {}".format(uid, uname, umail, target))
        return None
    print_debug("Cloud ID for {} ({}) is {}".format(uid, uname, cloudId))
    cuser = getCloudUser(target, cloudId, tuser, tpword)
    if not cuser:
        print_error("Could not find cloud user for AAID {} on {}".format(cloudId, target))
        return None
    print_verbose("Fetched user details for Cloud ID: {}\n{}".format(cloudId, cuser.json()))
    # For some reason, the following does not return email in the payload
    cmail = cuser.json()['email'].lower()
    cname = cuser.json()['displayName']
    # print_debug("Matching email for uid {} on {} is {}".format(uid, target, cmail))
    operations = cuser.json()['operations']
    if len(operations) <= 0:
        print_error("User [{}, {}] does not have required permissions on {}".format(cloudId, cmail, target))
        return None
    ops = []
    print_debug('User permissions found on cloud for server id {}: {}'.format(uid, cuser.json()['operations']))
    print_verbose('User {} has product access on Confluence Cloud and can:'.format(umail))
    for operation in operations:
        ops.append(operation['operation'])
    if cloudEmail and len(cloudEmail) > 0:
        print_debug("User {} has product access on Confluence Cloud as {}, and privileges to {} on {}".format(umail, cloudEmail, "/".join(ops), target))
    elif cname and len(cname) > 0:
        print_debug("User {} has product access on Confluence Cloud as {}, and privileges to {} on {}".format(umail, cname, "/".join(ops), target))
    else:
        print_debug("User {} has product access on Confluence Cloud as {}, and privileges to {} on {}".format(umail, cloudId, "/".join(ops), target))
    return [cloudId, cloudEmail]

def getCloudUser(target, uid, user, pword):
    # Example: https://kplahac.atlassian.net/wiki/rest/api/user?accountId=557058:f5ad2a44-8801-4121-85c5-847ba503f5de
    response = None
    params = urlencode({'accountId': uid})
    r = f'{makeSinkUrl(target)}/rest/api/user?{params}&expand=operations&expand=details.personal'
    print_verbose('Executing {}'.format(r))
    response = get_request(url=r, auth=(user, pword))
    if not response:
        print_error('Could not fetch user details from {}'.format(target))
        print_verbose('Error when user details from {} for user {}'.format(target, uid))
        return None
    else:
        return response

sourceEmailStore = {}


def getUserEmail(source, uname, user, pword):
    response = None
    if uname not in sourceEmailStore:
        print_verbose("Source: {}, Uname: {} ({})".format(source, uname, user))
        try:
            response = get_request(url=f'{makeSourceUrl(source)}/rest/mobile/1.0/profile/{uname}', auth=(user, pword)).json()
            sourceEmailStore[uname] = [response['email'], response['fullName']]
        except Exception as NoUsernameFound:
            print_verbose("No email found for user {} due to error:\n{}".format(uname, NoUsernameFound))
            return None
        sourceEmailStore[uname] = [response['email'], response['fullName']]
    return sourceEmailStore[uname]

def getUserEmailList(ulist, source, user, pword):
    if len(ulist) == 0:
        return []
    emails = []
    enames = []
    for uname in ulist:
        user_email = getUserEmail(source, uname, user, pword)
        emails.append(user_email[0].lower())
        enames.append(user_email[1])
    return emails, enames

keptFile = ""
fileIdStore = {}
emailMapStore = {}


def lookupFile(email, aaid, email_map=None):
    global keptFile
    global fileIdStore
    global emailMapStore
    searchEmail = email
    if emailMapStore and email in emailMapStore:
        searchEmail = emailMapStore[email]
    else:
        if email_map and len(email_map) > 0:
            with open(email_map) as emf:
                for num, line in enumerate(emf):
                    bits = line.split(',')
                    search = re.search('<([^>]+)>', bits[0], re.IGNORECASE)
                    if search is None:
                        search = re.search('([a-zA-Z0-9_.-]+[@][a-zA-Z0-9_.-]+)', bits[0], re.IGNORECASE)
                    if search is not None:
                        serverEmail = search.group(1)
                        cloudEmail = bits[1].strip()
                        emailMapStore[serverEmail] = cloudEmail
                        if serverEmail == searchEmail:
                            searchEmail = cloudEmail
    if not fileIdStore:
        if keptFile != aaid:
            with open(aaid) as f:
                for num, line in enumerate(f):
                    bits = line.split(',')
                    search = re.search('<([^>]+)>', bits[0], re.IGNORECASE)
                    if search is None:
                        search = re.search('([a-zA-Z0-9_.-]+[@][a-zA-Z0-9_.-]+)', bits[0], re.IGNORECASE)
                    if search is not None:
                        anEmail = search.group(1)
                        anId = bits[1].strip()
                        fileIdStore[anEmail] = anId
            keptFile = aaid
    if searchEmail in fileIdStore:
        return fileIdStore[searchEmail]
    return ''


sinkIdStore = {}


def getUser(sink, name, email, user, pword):
    global sinkIdStore
    if email not in sinkIdStore:
        response = None
        try:
            params = urlencode({'cql': f'user.fullname~{name}'})
            raw = get_request(url=f'{makeSinkUrl(sink)}/rest/api/search/user?{params}', auth=(user, pword))
            response = raw.json()
            if 'results' in response:
                for p in response['results']:
                    if len(p['user'].get('email', '')) > 0:
                        if p['user']['email'] == email:
                            sinkIdStore[email] = p['accountId']
        except requests.exceptions.RequestException as e:
            print_error("Could not lookup username '{}' on '{}'. Error: {}".format(name, sink, e))
        except Exception as e:
            print_error("Could not decode username '{}' from '{}'. Error: {}".format(name, sink, e))
    if email not in sinkIdStore:
        print_warning("'{}' is not a valid user on '{}'".format(name, sink))
        sinkIdStore[email] = ''
    return sinkIdStore[email]


def chunkit(list, n):
    for i in range(0, len(list), n):
        yield list[i:i + n]


mapCal = {}
mapUid = {}
CHUNKSIZE = 50
keyDict = {}
nameDict = {}
creatorDict = {}
spaceList = []
notMigratedSpaces = []

def readCalendars(file, space=None):
    aids = []
    global keyDict
    global nameDict
    global creatorDict
    sname = ''
    creator = ''
    if space:
        print_verbose('Reading calendars for {}'.format(space))
    for line in file:
        sname = creator = None
        columns = len(line.split(','))
        skey = line.split(',')[0].strip(QUOTES)
        calendar = line.split(',')[1].strip(QUOTES)
        print_verbose('skey = {}, calendar = {}'.format(skey, calendar))
        if columns > 2:
            sname = line.split(',')[2].strip(QUOTES)
            if columns > 3:
                creator = line.split(',')[3].strip().strip(QUOTES)
        if space and len(space) > 0:
            #found = re.search(rf"^\b{space}\b", line)
            #if found:
            if space == skey:
                aids.append(calendar)
                keyDict[calendar] = skey
                if sname:
                    nameDict[calendar] = sname
                if creator:
                    creatorDict[calendar] = creator
        else:
            aids.append(calendar)
            if skey and len(skey) > 0:
                keyDict[calendar] = skey
            if sname:
                nameDict[calendar] = sname
            if creator:
                creatorDict[calendar] = creator
    return aids

def fakeReadCalendars(list):
    aids = []
    global keyDict
    global nameDict
    for calendar in list:
        aids.append(calendar)
        keyDict[calendar] = ''
        nameDict[calendar] = ''
    return aids

# this has to be done after the calendar (& events) is created on the cloud side
def restrictOneCalendar(calId, cal, uid, source, source_user, source_password, target, target_user, target_password, aaid):
    responseDict = {
        "spaceStatus": "",
        "calendarStatus": "",
        "eventCount": 0,
        "failedEventCount": 0,
        "reason": ""
    }
    print_verbose(calId)
    try:
        sinkCals = getSomeCalendarsSink(target, [calId], target_user, target_password)
        if sinkCals:
        # there should be only one calendar ...
            for oneCal in sinkCals:
                subcals = []
                if 'childSubCalendars' in oneCal:
                    for sub in oneCal['childSubCalendars']:
                        subcals.append(sub['subCalendar']['id'])
                    calRestrict = makeRestrict(cal, cal['subCalendar'], calId, subcals, uid, source, source_user, source_password, target, target_user, target_password, aaid=aaid)
                    print_verbose("calRestrict: {}".format(calRestrict))
                    putRestrict(calRestrict, uid, target, target_user, target_password)
            responseDict['calendarStatus'] = 'migrated'
    except ValueError as m:
        print_warning("Could not create restrictions on calendar '{}'.".format(cal['subCalendar']['name']))
        responseDict['calendarStatus'] = 'no restrictions'
        responseDict['reason'] = m
    except Exception as e:
        print_verbose("Generic error occurred restricting calendar {}\n{}".format(cal['subCalendar']['name'], e))
        responseDict['calendarStatus'] = 'ignored'
    return responseDict

def processOneCalendar(cal, space, source, source_user, source_password, target, target_user, target_password, aaid, default_creator, retain, source_appid, target_appid, preflight=False, email_map=None, start_date=None):
    global mapCal
    global mapUid
    global dateFormat
    global timeFormat
    global stopOnFirstError
    global outputFile
    global noOutput
    global defaultInvitee
    global keyDict
    global nameDict
    uid = ''
    skippedSpaces = []
    missingSpaces = []
    responseDict = {
        "spaceStatus": "",
        "calendarStatus": "",
        "eventCount": 0,
        "failedEventCount": 0,
        "reason": "",
        "creator": "",
        "calId": ""
    }
    uid = cal['subCalendar']['creator']
    username = None
    if uid == 'creator':
        print_verbose("Skipping SubCalendar: {}".format(cal))
        responseDict['calendarStatus'] = 'skipped'
    else:
        if 'spaceKey' in cal['subCalendar']:
            spaceKey = cal['subCalendar']['spaceKey']
            if space is not None and spaceKey.upper() != space.upper():
                if spaceKey not in skippedSpaces:
                    print_debug("Skipping space {}".format(spaceKey))
                    skippedSpaces.append(spaceKey)
                responseDict['spaceStatus'] = 'skipped'
                responseDict['calendarStatus'] = 'not migrated'
                responseDict['reason'] = 'Space not marked for team calendars migration'
                return responseDict
            else:
                if spaceKey not in missingSpaces:
                    print_info("Checking calendar {} in space {}".format(cal['subCalendar']['name'], spaceKey))
                    try:
                        username = getUserName(source, uid, source_user, source_password)
                    except Exception as err5:
                        print_warning("Could not fetch creator's details for calendar '{}' from {} (uid: {})".format(cal['subCalendar']['name'], source, uid))
                        print_verbose("Error when fetching username for uid '{}':\n{}\nCalendar payload: {}".format(uid, err5, cal['subCalendar']))
                else:
                    responseDict['spaceStatus'] = 'missing'
                    responseDict['calendarStatus'] = 'not migrated'
                    responseDict['reason'] = 'Space does not exist on target cloud site'
                    return responseDict
    try:
        creator = ''
        if not username:
            raise
        useremail = getUserEmail(source, username, source_user, source_password)[0]
        useremail = useremail.lower()
        print_verbose("User Email: {} (Server)".format(useremail))
        if len(useremail) > 0:
            if aaid and len(aaid) > 0:
                try:
                    creator = lookupFile(useremail, aaid, email_map)
                except Exception:
                    print_warning("Error looking up Atlassian ID from aaid file '{}' for email '{}'".format(aaid, useremail))
                    responseDict['calendarStatus'] = 'failed'
                    responseDict['spaceStatus'] = 'found'
                    return responseDict
            else:
                try:
                    creator = getUser(target, username, useremail, target_user, target_password)
                except Exception:
                    print_warning("Error looking up Atlassian ID from target site '{}' for email '{}'".format(target, useremail))
                    responseDict['calendarStatus'] = 'failed'
                    responseDict['spaceStatus'] = 'found'
                    return responseDict
            print_verbose("Calendar creator: '{}'".format(creator))
            if len(creator) > 0:
                mapUid[uid] = creator
            else:
                raise
        else:
            raise
    except:
        print_warning("Could not fetch email for user '{}', the creator of calendar '{}', from {}".format(uid, cal['subCalendar']['name'], source))
        # If the --default-creator parameter is passed, create the calendar as that cloud user
        # If the --default-creator parameter is NOT passed, create the calendar as the user running the script (--target-user)
        if len(default_creator) > 0:
            print_warning("Creating the calendar as '{}' (default creator)".format(default_creator))
            new_creator = default_creator
        else:
            print_warning("Creating the calendar as '{}' (script runner)".format(target_user))
            new_creator = target_user
        creator = lookupFile(new_creator, aaid)
        if len(creator) == 0:
            print_error("Could not find Atlassian ID for '{}' in '{}' file.".format(new_creator, aaid))
            responseDict['calendarStatus'] = 'failed'
            responseDict['spaceStatus'] = 'found'
            responseDict['reason'] = 'Calendar creator not found'
            return responseDict
        useremail = new_creator
    print_verbose("Calendar name: {}, creator email: {}, creator AAID: {}".format(cal['subCalendar']['name'], useremail, creator))
    if len(creator) > 0:
        try:
            calDict = makeCal(cal['subCalendar'], creator)
            print_verbose("calDict: {}".format(calDict))
            calId = putCalendar(calDict, creator, target, target_user, target_password)
            if not calId:  # space does not exist on target
                raise ValueError('Space does not exist on target')
            responseDict['spaceStatus'] = 'found'
            responseDict['calendarStatus'] = 'migrated'
            responseDict['creator'] = creator
            responseDict['calId'] = calId
        except ValueError as m:
            print_warning("Could not create calendar '{}' in space '{}'. Please check:\n(1) The space exists on target cloud site, {}\n(2) The user {} has product access to Confluence and on space {}\n".format(calDict['name'], calDict['spaceKey'], target,useremail+'/'+creator,calDict['spaceKey']))
            if not DEVMODE or not DEBUG:
                print_warning("Please re-run the script with --debug and --verbose options and share the results with Atlassian Support.")
            missingSpaces.append(space)
            responseDict['spaceStatus'] = 'missing'
            responseDict['calendarStatus'] = 'not migrated'
            responseDict['reason'] = m
            return responseDict
        for ctype in cal['subCalendar']['customEventTypes']:
            putCustomType(makeType(calId, ctype), target, target_user, target_password,ctype['customEventTypeId'])
        print_info("Created calendar '{}' successfully.".format(cal['subCalendar']['name']))
        print_info("Created calendar {} for user {} as {} (DB update)".format(calId, useremail, creator))
        mapCal[cal['subCalendar']['id']] = calId
        recurEventArray = []
        for u in cal.get('childSubCalendars', []):
            events = getEvents(u['subCalendar']['id'], source, source_user, source_password, start_date)
            if not events:
                print_debug("No more events fetched for calendar '{}'".format(u['subCalendar']['name']))
                # Delete the calendar from cloud site here
                return responseDict
            print_info("{} event(s) to create in calendar '{}'".format(countEvents(events), u['subCalendar']['name']))
            for ev in events:
                if 'recur' in ev:
                    recurEventId = ev['id']
                    try:
                        evIDList = recurEventId.split('/')
                        uniqId = "-".join(evIDList[1:])
                        if uniqId in recurEventArray:
                            continue
                        else:
                            recurEventArray.append(uniqId)
                    except ValueError as ve:
                        print_error("Could not create event {} due to invalid event ID: {}".format(ev['title'], recurEventId))
                        continue
                try:
                    eventDict = makeEvent(calId, ev, uid, target, target_user, target_password, source_appid, target_appid, aaid=aaid)
                    if not eventDict:
                        print_error('Could not setup event payload for {}'.format(ev))
                        responseDict['failedEventCount'] += 1
                except Exception as eventError:
                    print_error("Could not prepare payload for event '{}' in calendar '{}' due to error: {}".format(ev['title'],calId,eventError))
                    continue
                try:
                    print_verbose("eventDict: {}".format(eventDict))
                    if eventDict:
                        saveEvent = eventDict
                        result = putEvent(eventDict, uid, target, target_user, target_password, aaid)
                        if result:
                            responseDict['eventCount'] += 1
                        else:
                            responseDict['failedEventCount'] += 1
#                            writeICS(saveEvent)
                except Exception as err1:
                    print_error("Could not migrate event '{}' to calendar '{}' due to error: {}".format(ev['title'], calId,err1))
                    responseDict['failedEventCount'] += 1
#                        writeICS(saveEvent)
        if not retain:
            if not hideCalendar(makeHide(calId), target, target_user, target_password):
                print_debug("Could not hide calendar {}".format(calId))
    else:
        print_warning("Calendar '{}' not migrated. Calendar creator email blank or invalid.".format(cal['subCalendar']['name']))
        responseDict['spaceStatus'] = 'found'
        responseDict['calendarStatus'] = 'failed'
        responseDict['reason'] = 'Calendar creator is blank or invalid'
    return responseDict

def putAnalytics(cloudid, calendars_migrated):
    global VERSION
    response = None
    myheaders = { 'Content-type': 'application/json' }
    time_saved = (calendars_migrated * 15)/60
    data = { "cloudid": cloudid, "calendars_migrated": calendars_migrated, "time_saved": time_saved, "version": VERSION }
    print_verbose(data)
    r = "https://automation.atlassian.com/pro/hooks/7c6f110121ace61057246f07012fef51c2920aed"
    response = requests.post(r, headers=myheaders, data=json.dumps(data))

def preflight_checks(source, target, source_user, source_password, target_user, target_password, input_file, aaid, space="", date_format="", time_format="", stop_on_error=False, logfile="", no_output=False, default_invitee="", default_creator="", source_appid=None, target_appid=None, retain=True, pagesize=-1, startat=-1, confirm=False, debug=False, verbose=False, preflight=True, email_map=None, start_date=None):
    global mapCal
    global mapUid
    global timeFormat
    global stopOnFirstError
    global outputFile
    global noOutput
    global defaultInvitee
    global keyDict
    global nameDict
    global spaceList
    global notMigratedSpaces
    init_rich()
    noOutput = no_output
    cloudName = target.split('.')[0].split('/')[2]
    asnow = datetime.now().strftime("%Y-%m-%d_%H%M")
    if space and len(space) > 0:
        logFile = f'{cloudName}_{space}_preflight_{asnow}.log'
    else:
        logFile = f'{cloudName}_full_preflight_{asnow}.log'
    if logfile:
        logFile = logfile
    if not outputFile:
        outputFile = open(logFile, "a")
    show_version()
    stopOnFirstError = stop_on_error
    defaultInvitee = default_invitee
    invalidCalendarsFile = f'invalid_calendars_{asnow}.csv'
    verifiedCalendarsFile = f'verified_calendars_{asnow}.csv'
    invalidFile = open(invalidCalendarsFile, 'w')
    migratedFile = open(verifiedCalendarsFile, 'w')
    invalidWriter = csv.writer(invalidFile)
    migratedWriter = csv.writer(migratedFile)
    if len(input_file) > 0:
        i = open(input_file)
        if space and len(space) > 0:
            ids = readCalendars(i, space)
        else:
            ids = readCalendars(i)
    else:
        if not startat or startat < 0:
            startat = 0
        if not pagesize or pagesize < 1:
            pagesize = 10000
        if space:
            ids = fakeReadCalendars(getAllCalendars(source, startat, pagesize, source_user, source_password, space))
        else:
            ids = fakeReadCalendars(getAllCalendars(source, startat, pagesize, source_user, source_password))
    aids = ids
    if startat and pagesize:
        if startat >= 0 and pagesize > 0:
            aids = ids[startat * pagesize: (startat + 1) * pagesize]
    eventCount = 0
    calendarCount = 0
    failedCalendarCount = 0
    restrictionCount = 0
    invalidUserCount = 0
    invalidUsers = []
    print_info("Pre-flight checks for migration of team calendars from {} (source) to {} (target) started. Output log: {}".format(source, target, logFile))
    if getCloudSpaces(target, target_user, target_password) == 0:
        return "No space found on target cloud site {}. Cannot continue.".format(target)
    if space and len(space) > 0:
        print_info("Migrating space '{}' only.".format(space))
        if space not in spaceList:
            return "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(space)
    for c in chunkit(aids, CHUNKSIZE):
        result = getSomeCalendars(source, c, source_user, source_password)
        if not result:
            print_verbose("One of more of the following calendar(s) could not be fetched from server: {}".format(c))
            for c1 in chunkit(c,1):
                result1 = getSomeCalendars(source, c1, source_user, source_password)
                if not result1:
                    calID = c1[0]
                    if calID in keyDict:
                        debug_rec = f'{keyDict[calID]},{calID}'
                    else:
                        debug_rec = f',{calID}'
                    if nameDict and calID in nameDict:
                        debug_rec = debug_rec + f',{nameDict[calID]}'
                    print_debug(f"Details for calendar {debug_rec} could not be fetched from server.")
                    print_warning(f'Calendar ({debug_rec}) is invalid and will not be migrated.')
                    invalidWriter.writerow([debug_rec])
                    failedCalendarCount +=1
                else:
                    for cal in result1:
                        cloudId = cloudEmail = None
                        calID = cal['subCalendar']['id']
                        uid = cal['subCalendar']['creator']
                        if uid == 'creator':
                            continue
                        if not calID in keyDict:
                            print_warning("There is no space associated with calendar {}".format(cal['subCalendar']['name']))
                        if uid not in invalidUsers:
                            validOrNot = validateCloudUser(source, target, uid, source_user, source_password, target_user,
                                                    target_password,
                                                    aaid, email_map, default_creator)
                            if validOrNot:
                                cloudId = validOrNot[0]
                                cloudEmail = validOrNot[1]
                        else:
                            if uid in cloudUsers:
                                print_error("Could not find AAID (Atlassian ID) for user: [{}, {}, {}] on {}".format(uid, cloudUsers[uid]['uname'], cloudUsers[uid]['email'], target))
                            else:
                                print_error("Could not find AAID (Atlassian ID) for user: [{}, {}, {}] on {}".format(uid, target))
                        if not cloudId:
                            if calID in keyDict:
                                cal_rec = f'{keyDict[calID]},{calID}'
                            else:
                                cal_rec = f',{calID}'
                            if nameDict and calID in nameDict:
                                cal_rec = cal_rec + f',{nameDict[calID]}'
                            invalidWriter.writerow([cal_rec])
                            print_warning(f"Calendar ({cal_rec}) will not be migrated.")
                            invalidUserCount += 1
                            invalidUsers.append(uid)
                        else:
                            if len(keyDict[calID]) > 0:
                                skey = keyDict[calID]
                            else:
                                skey = '~' + cloudId
                                keyDict[calID] = skey
                            if skey in notMigratedSpaces:
                                continue
                            if skey not in spaceList:
                                notMigratedSpaces.append(skey)
                                if '~' in skey:
                                    print_warning(
                                        "Personal space {} does not exist on cloud. Cannot migrate any calendars for this space/user.".format(
                                            skey))
                                else:
                                    print_warning(
                                        "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(
                                            skey))
                                continue
                            cal_rec = f'{keyDict[calID]},{calID}'
                            if nameDict and calID in nameDict:
                                cal_rec = cal_rec + f',{nameDict[calID]}'
                            if creatorDict and calID in creatorDict:
                                cal_rec = cal_rec + f',{creatorDict[calID]}'
                            migratedWriter.writerow([cal_rec])
                            if cloudEmail and len(cloudEmail) > 0:
                                print_info(f"Calendar {cal_rec} will be migrated as user {cloudId} ({cloudEmail}) on {target}.")
                            else:
                                print_info(f"Calendar {cal_rec} will be migrated as user {cloudId} on {target}.")
                            calEvents = 0
                            try:
                                for u in cal.get('childSubCalendars', []):
                                    events = getEvents(u['subCalendar']['id'], source, source_user, source_password)
                                    calEvents += countEvents(events)
                            except Exception as e:
                                print_debug("1: Could not fetch events from {}. Error:\n{}".format(source, e))
                                continue
                            print_info("{} events found in calendar {}".format(calEvents, nameDict[calID])) 
                            
        else:
            for cal in result:
                cloudId = cloudEmail = None
                calID = cal['subCalendar']['id']
                uid = cal['subCalendar']['creator']
                if uid == 'creator':
                    continue
                if not calID in keyDict:
                    print_warning(
                        "There is no space associated with calendar {}.".format(cal['subCalendar']['name']))
                if uid not in invalidUsers:
                    validOrNot = validateCloudUser(source, target, uid, source_user, source_password, target_user,
                                                   target_password,
                                                   aaid, email_map, default_creator)
                    if validOrNot:
                        cloudId = validOrNot[0]
                        cloudEmail = validOrNot[1]
                else:
                    if uid in cloudUsers:
                        print_error("Could not find AAID (Atlassian ID) for user: [{}, {}, {}] on {}".format(uid, cloudUsers[uid]['uname'],cloudUsers[uid]['email'],target))
                    else:
                        print_error("Could not find AAID (Atlassian ID) for user: {} on {}".format(uid, target))
                if not cloudId:
                    if calID in keyDict:
                        cal_rec = f'{keyDict[calID]},{calID}'
                    else:
                        cal_rec = f',{calID}'
                    if nameDict and calID in nameDict:
                        cal_rec = cal_rec + f',{nameDict[calID]}'
                    invalidWriter.writerow([cal_rec])
                    print_warning(f"Calendar ({cal_rec}) will not be migrated.")
                    invalidUserCount += 1
                    invalidUsers.append(uid)
                else:
                    if calID in keyDict and keyDict[calID]:
                        skey = keyDict[calID]
                    else:
                        skey = '~' + cloudId
                        keyDict[calID] = skey
                    if skey in notMigratedSpaces:
                        continue
                    if skey not in spaceList:
                        notMigratedSpaces.append(skey)
                        if '~' in skey:
                            print_warning(
                                "Personal space {} does not exist on cloud. Cannot migrate any calendars for this space/user.".format(skey))
                        else:
                            print_warning(
                                "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(skey))
                        continue
                    cal_rec = f'{keyDict[calID]},{calID}'
                    if nameDict and calID in nameDict:
                        cal_rec = cal_rec + f',{nameDict[calID]}'
                    if creatorDict and calID in creatorDict:
                        cal_rec = cal_rec + f',{creatorDict[calID]}'
                    migratedWriter.writerow([cal_rec])
                    if cloudEmail and len(cloudEmail) > 0:
                        print_info(f"Calendar {cal_rec} will be migrated as user {cloudId} ({cloudEmail}) on {target}.")
                    else:
                        print_info(f"Calendar {cal_rec} will be migrated as user {cloudId} on {target}.")
                    calEvents = 0
                    try:
                        for u in cal.get('childSubCalendars', []):
                            events = getEvents(u['subCalendar']['id'], source, source_user, source_password)
                            calEvents += countEvents(events)
                    except Exception as e:
                        print_debug("2: Could not fetch events from {}. Error:\n{}".format(source, e))
                        continue
                    print_info("{} events found in calendar {}".format(calEvents, nameDict[calID])) 
    if invalidUserCount == 0:
        print_info("All calendar creators/owners, from {}, validated on {}".format(source, target))
    if space and len(space) > 0:
        return f"Preflight checks for migration of team calendars from {source} to {target} for space '{space}' finished. Invalid users: {invalidUserCount}. Invalid calendars written to {invalidCalendarsFile} file."
    else:
        return f'Preflight checks for migration of team calendars from {source} to {target} finished. Invalid users: {invalidUserCount}. Invalid calendars in {invalidCalendarsFile} file.'


def migrate(source, target, source_user, source_password, target_user, target_password, input_file, aaid, space="", date_format="", time_format="", stop_on_error=False, logfile="", no_output=False, default_invitee="", default_creator="", source_appid=None, target_appid=None, retain=True, pagesize=-1, startat=-1, confirm=False, preflight=False, debug=False, verbose=False, email_map=None, start_date=None):
    global mapCal
    global mapUid
    global dateFormat
    global timeFormat
    global stopOnFirstError
    global outputFile
    global noOutput
    global defaultInvitee
    global keyDict
    global nameDict
    global notMigratedSpaces
    init_rich()
    noOutput = no_output
    cloudName = target.split('.')[0].split('/')[2]
    asnow = datetime.now().strftime("%Y-%m-%d_%H%M")
    if space and len(space) > 0:
        logFile = f'{cloudName}_{space}_migration_{asnow}.log'
    else:
        logFile = f'{cloudName}_full_migration_{asnow}.log'
    if logfile:
        logFile = logfile
    if not outputFile:
        outputFile = open(logFile, "a")
    show_version()
    stopOnFirstError = stop_on_error
    defaultInvitee = default_invitee
    invalidCalendarsFile = f'invalid_calendars_{asnow}.csv'
    migratedCalendarsFile = f'migrated_calendars_{asnow}.csv'
    invalidFile = open(invalidCalendarsFile, 'w')
    migratedFile = open(migratedCalendarsFile, 'w')
    invalidWriter = csv.writer(invalidFile)
    migratedWriter = csv.writer(migratedFile)
    if len(dateFormat) == 0:
        dateFormat = date_format
    if len(timeFormat) == 0:
        timeFormat = time_format
    if len(input_file) > 0:
        i = open(input_file)
        if space and len(space) > 0:
            ids = readCalendars(i, space)
        else:
            ids = readCalendars(i)
    else:
        if not startat or startat < 0:
            startat = 0
        if not pagesize or pagesize < 1:
            pagesize = 10000
        if space:
            ids = fakeReadCalendars(getAllCalendars(source, startat, pagesize, source_user, source_password, space))
        else:
            ids = fakeReadCalendars(getAllCalendars(source, startat, pagesize, source_user, source_password))
    aids = ids
    if startat and pagesize:
        if startat >= 0 and pagesize > 0:
            aids = ids[startat * pagesize: (startat + 1) * pagesize]
    eventCount = 0
    calendarCount = 0
    failedEventCount = 0
    failedCalendarCount = 0
    restrictionCount = 0
    invalidUserCount = 0
    print_info("Migration of team calendars from {} (source) to {} (target) started. Output log: {}".format(source, target, logFile))
    if getCloudSpaces(target, target_user, target_password) == 0:
            return "No space found on target cloud site {}. Cannot continue.".format(target)
    if space and len(space) > 0:
        print_info("Migrating space '{}' only.".format(space))
        if space not in spaceList:
            return "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(space)
#    chunks = chunkit(aids, CHUNKSIZE);
    print_verbose('aids = {}'.format(aids))
    for c in chunkit(aids, CHUNKSIZE):
        print_verbose("c = {}".format(c))
#    for c in chunks:
        result = getSomeCalendars(source, c, source_user, source_password)
        if not result:
            print_verbose("One of more of the following calendar(s) could not be fetched from server: {}".format(c))
            for c1 in chunkit(c,1):
                cloudId = cloudEmail = None
                result1 = getSomeCalendars(source, c1, source_user, source_password)
                if not result1:
                    calID = c1[0]
                    if calID in keyDict:
                        debug_rec = f'{keyDict[calID]},{calID}'
                    if nameDict and calID in nameDict:
                        debug_rec = debug_rec + f',{nameDict[calID]}'
                    print_debug(f"Details for calendar {debug_rec} could not be fetched from server.")
                    invalidWriter.writerow([debug_rec])
                    failedCalendarCount +=1
                else:
                    for cal in result1:
                        calID = cal['subCalendar']['id']
                        uid = cal['subCalendar']['creator']
                        if uid == 'creator':
                            continue
                        if not calID in keyDict:
                            print_warning("There is no space associated with calendar {}".format(cal['subCalendar']['name']))
                        validOrNot = validateCloudUser(source, target, uid, source_user, source_password, target_user,
                                                    target_password,
                                                    aaid, email_map, default_creator)
                        if validOrNot:
                            cloudId = validOrNot[0]
                            cloudEmail = validOrNot[1]
                        if not cloudId:
                            if calID in keyDict:
                                cal_rec = f'{keyDict[calID]},{calID}'
                            else:
                                cal_rec = f',{calID}'
                            if nameDict and calID in nameDict:
                                cal_rec = cal_rec + f',{nameDict[calID]}'
                            invalidWriter.writerow([cal_rec])
                            print_warning(f"Calendar ({cal_rec}) will not be migrated.")
                            invalidUserCount += 1
                        else:
                            if len(keyDict[calID]) > 0:
                                skey = keyDict[calID]
                            else:
                                skey = '~' + cloudId
                                keyDict[calID] = skey
                            if skey in notMigratedSpaces:
                                continue
                            if skey not in spaceList:
                                notMigratedSpaces.append(skey)
                                if '~' in skey:
                                    print_warning(
                                        "Personal space {} does not exist on cloud. Cannot migrate any calendars for this space/user.".format(
                                            skey))
                                else:
                                    print_warning(
                                        "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(
                                            skey))
                                continue
                            calResponse = processOneCalendar(cal, space, source, source_user, source_password, target, target_user, target_password, aaid, default_creator, retain, source_appid, target_appid, start_date=start_date)
                            writerRow = [keyDict[calID], calID]
                            if nameDict and nameDict[calID]:
                                writerRow.append(nameDict[calID])
                            if calResponse['calendarStatus'] == 'migrated':
                                calendarCount += 1
                                migratedWriter.writerow(writerRow)
                            else:
                                failedCalendarCount += 1
                                invalidWriter.writerow(writerRow)
                            failedEventCount += calResponse['failedEventCount']
                            eventCount += calResponse['eventCount']
        else:
            for cal in result:
                cloudId = cloudEmail = None
                calID = cal['subCalendar']['id']
                uid = cal['subCalendar']['creator']
                if uid == 'creator':
                    continue
                if not calID in keyDict:
                    print_warning("There is no space associated with calendar {}".format(cal['subCalendar']['name']))
                validOrNot = validateCloudUser(source, target, uid, source_user, source_password, target_user,
                                            target_password,
                                            aaid, email_map, default_creator)
                if validOrNot:
                    cloudId = validOrNot[0]
                    cloudEmail = validOrNot[1]
                if not cloudId:
                    if calID in keyDict:
                        cal_rec = f'{keyDict[calID]},{calID}'
                    else:
                        cal_rec = f',{calID}'
                    if nameDict and calID in nameDict:
                        cal_rec = cal_rec + f',{nameDict[calID]}'
                    invalidWriter.writerow([cal_rec])
                    print_warning(f"Calendar ({cal_rec}) will not be migrated.")
                    invalidUserCount += 1
                else:
                    if len(keyDict[calID]) > 0:
                        skey = keyDict[calID]
                    else:
                        skey = '~' + cloudId
                        keyDict[calID] = skey
                    if skey in notMigratedSpaces:
                        continue
                    if skey not in spaceList:
                        notMigratedSpaces.append(skey)
                        if '~' in skey:
                            print_warning(
                                "Personal space {} does not exist on cloud. Cannot migrate any calendars for this space/user.".format(
                                    skey))
                        else:
                            print_warning(
                                "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(
                                    skey))
                        continue
                    calResponse = processOneCalendar(cal, space, source, source_user, source_password, target, target_user, target_password, aaid, default_creator, retain, source_appid, target_appid, start_date=start_date)
                    writerRow = [keyDict[calID], calID]
                    if nameDict and nameDict[calID]:
                        writerRow.append(nameDict[calID])
                    if calResponse['calendarStatus'] == 'migrated':
                        calendarCount += 1
                        migratedWriter.writerow(writerRow)
                    else:
                        failedCalendarCount += 1
                        invalidWriter.writerow(writerRow)
                    failedEventCount += calResponse['failedEventCount']
                    eventCount += calResponse['eventCount']
    if failedCalendarCount > 0 or failedEventCount > 0:
        print_debug("{}/{} calendars could not be created. {}/{} events could not be created.".format(failedCalendarCount, failedCalendarCount + calendarCount, failedEventCount, failedEventCount + eventCount))
    if calendarCount > 0:
        putAnalytics(target, calendarCount)
    if space and len(space) > 0:
        return f"Migration of team calendars from {source} to {target} for space {space} finished (migrated {eventCount} events and {restrictionCount} restrictions in {calendarCount} calendars). Invalid calendars writen to {invalidCalendarsFile} file."
    else:
        return f'Migration of team calendars from {source} to {target} finished (migrated {eventCount} events and {restrictionCount} restrictions in {calendarCount} calendars). Invalid calendars written to {invalidCalendarsFile} file.'


def noslash(url):
    return url.strip('/')

console = None
reset_style = None
info_style = None
debug_style = None
error_style = None
warning_style = None

def init_rich():
    global console
    global info_style
    global debug_style
    global error_style
    global warning_style
    console = Console(record=True)
    info_style = Style(color="white", bold=True)
    debug_style = Style(color="cyan", bold=True)
    warning_style = Style(color="yellow", bold=True)
    error_style = Style(color="red", bold=True, italic=True)
    reset_style = Style(color="white", bold=False)


def get_cloud_user_by_server_id(uid, source, source_user, source_password, target, target_user, target_password, aaid=""):
    global mapUid
    if uid in mapUid:
        return mapUid[uid]

    try:
        username = getUserName(source, uid, source_user, source_password)
    except Exception as err5:
        print_warning("Could not fetch username for uid '{}', from {}".format(uid, source))
        print_verbose("Error when fetching username for uid '{}':\n{}".format(uid, err5))

    try:
        cloud_user = ''
        if not username:
            raise
        useremail = getUserEmail(source, username, source_user, source_password)[0]
        useremail = useremail.lower()
        print_verbose("User Email: {}".format(useremail))
        if len(useremail) > 0:
            if aaid and len(aaid) > 0:
                try:
                    cloud_user = lookupFile(useremail, aaid)
                except Exception:
                    print_warning(
                        "Error looking up Atlassian ID from aaid file '{}' for email '{}'".format(aaid, useremail))
                    return None
            else:
                try:
                    cloud_user = getUser(target, username, useremail, target_user, target_password)
                except Exception:
                    print_warning(
                        "Error looking up Atlassian ID from target site '{}' for email '{}'".format(target, useremail))
                    return None
            if len(cloud_user) > 0:
                mapUid[uid] = cloud_user
            else:
                raise
        else:
            raise
    except:
        print_warning("Could not fetch email for user '{}', from {}".format(uid, source))


def permissions(source, target, source_user, source_password, target_user, target_password, space="", aaid="", date_format="", time_format="", stop_on_error=False, logfile="", input_file="", no_output=False, default_invitee="", default_creator="", source_appid=None, target_appid=None, retain=True, pagesize=-1, startat=-1, confirm=False, debug=False, verbose=False, preflight=False, email_map=None, start_date=None):
    global mapCal
    global mapUid
    global dateFormat
    global timeFormat
    global stopOnFirstError
    global outputFile
    global noOutput
    global defaultInvitee
    failedPermissionCount = 0
    permissionCount = 0
    noOutput = no_output
    if len(dateFormat) == 0:
        dateFormat = date_format
    if len(timeFormat) == 0:
        timeFormat = time_format
    stopOnFirstError = stop_on_error
    defaultInvitee = default_invitee
    cloudName = target.split('.')[0].split('/')[2]
    asnow = datetime.now().strftime("%Y-%m-%d_%H%M")
    if space and len(space) > 0:
        logFile = f'{cloudName}_{space}_migration_{asnow}.log'
    else:
        logFile = f'{cloudName}_full_migration_{asnow}.log'
    if logfile:
        logFile = logfile
    if not outputFile:
        outputFile = open(logFile, "a")

    print_info("Migration of user permissions from {} to {} started. Output log: {}".format(source, target, logFile))

    userUpto = 0
    userChunk = 100
    hasMore = True
    invalidUserCount = 0
    while hasMore:
        try:
            users = getCalendarUsers(source, userUpto, userChunk, source_user, source_password)
            if not users or 'payload' not in users:
                users = {
                    'payload': []
                }
        except Exception as errU:
            users = {
                'payload': []
            }
            print_debug("Could not get users at {} due to error {}".format(userUpto, errU))
        for uid in users['payload']:
            if not validateCloudUser(source, target, uid, source_user, source_password, target_user, target_password, aaid, email_map, default_creator):
                print_error('User {} does not exist on {}'.format(uid, target))
                invalidUserCount += 1
                continue
            else:
                if preflight:
                    continue
            failure = False
            tUserId = None
            try:
                permission = getUserPermissions(uid, source, source_user, source_password)
                if permission:
                    try:
                        tUserId = get_cloud_user_by_server_id(uid, source, source_user, source_password, target, target_user, target_password, aaid) #  mapUid[uid]
                    except:
                        tUserId = None
            except Exception as errP:
                print_error("Could not get user permissions for {} due to error {}".format(uid, errP))

            # if we don't have the user, it may be in a space not migrated
            if tUserId:
                print_debug('tUserId = {}'.format(tUserId))
                if not putPermissionView(tUserId, permission['view'], target, target_user, target_password):
                    failure = True
                for cal in permission['subCalendarsInView']:
                    if cal in mapCal:
                        tCal = mapCal[cal]
                        # if we don't have the calendar, it may be in a space not migrated
                        if tCal:
                            try:
                                if not putWatch(tUserId, tCal, target, target_user, target_password):
                                    failure = True
                            except Exception as err1:
                                failure = True
                                print_error("Could not migrate watch permission for '{}' to calendar {} due to error {}".format(tUserId, tCal, err1))
                    else:
                        print_warning("Could not migrate watch permission for '{}' to calendar {} because it doesn't exist".format(tUserId, cal))
                for cal in permission['disabledMessageKeys']:
                    # if we don't have the calendar, it may be in a space not migrated
                    try:
                        if not putDisableKey(tUserId, cal, target, target_user, target_password):
                            failure = True
                    except Exception as err2:
                        failure = True
                        print_error("Could not migrate key permission for '{}' of {} due to error {}".format(tUserId, cal, err2))
                watchedCals = []
                for cal in permission['watchedSubCalendars']:
                    if cal in mapCal:
                        tCal = mapCal[cal]
                        # if we don't have the calendar, it may be in a space not migrated
                        if tCal:
                            watchedCals.append(tCal)
                    else:
                        print_warning("Could not migrate watch for '{}' to calendar {} because it doesn't exist".format(tUserId, cal))
                try:
                    if not putInView(tUserId, watchedCals, target, target_user, target_password):
                        failure = True
                except Exception as err3:
                    failure = True
                    print_error(
                        "Could not migrate subscribe permission for '{}' to calendars due to error {}".format(tUserId, err3))
                if failure:
                    failedPermissionCount += 1
                permissionCount += 1
            else:
                print_warning('Could not find matching user in cloud for uid = {}'.format(uid))
        userUpto += len(users['payload'])
        hasMore = len(users['payload']) - userChunk >= 0
    if invalidUserCount == 0:
        print_info("All users with permissions, from {}, validated on {}".format(source, target))
    if failedPermissionCount > 0:
        print_debug("{}/{} permissions could not be migrated.".format(failedPermissionCount, failedPermissionCount + permissionCount))
    if preflight:
        return f'Preflight checks for migration of user permissions from {source} to {target} finished.'
    return f'Migration of team calendar permissions from {source} to {target} finished (migrated permissions for {permissionCount} users.)'


def main():
    os.environ["PYTHONIOENCODING"] = 'utf-8' # this is to handle printing of unicode characters (such as in calendar names, etc)
    init_rich()
    if platform.system() == 'Windows':
        print_error('Windows is not supported as a platform for this script. Please try Linux or MacOS as tested platforms.\nYou could try with an emulator such as Cygwin but the results may be unpredicatable.')
        exit(1)
    args = parse_arguments()
    if validateUrl(args.source) and validateUrl(args.target):
        print_debug('Source {} and Target {} validated!'.format(args.source, args.target))
        if args.preflight:
            migration_status = preflight_checks(**vars(args))
        else:
            migration_status = migrate(**vars(args))
        print_info(migration_status)
        permission_status = permissions(**vars(args))
        print_info(permission_status)


if __name__ == '__main__':
    main()
