# README

## Bugs fixed

### Release 2.3
- Set minimum python version to 3.8+ due to datetime.fromisoformat implementation

### Release 2.1
- Fixed --start-date parameter default value which was causing the following error:
```python
TypeError: expected string or bytes-like object
```
- Fixed extraction of unique event IDs for recurring events

### Release 2.0
- Fixed url encoding issue in getUser() function

## New Features

### Release 1.5

- Preflight checks for team calendars migration script. Use the option ```--preflight``` to run preflight checks.

### What do the preflight checks do?

- Check if required spaces exist on cloud (NOTE: this script does not migrate spaces. Use CCMA for that)
- Check if calendar owners and users (ones that have permissions on calendars) are valid on cloud, and have the required permissions

Using the output log, following can be done before re-running the script:

- Migrate spaces that contain calendars but are not on cloud
- Migrate users and/or provide required access to users on cloud, such as product access to Confluence, space permissions, and so on.

## Introduction

Confluence Team Calendars are not migrated along with the Confluence Cloud Migration Assistant. This script fills the gap by automating migration of these calendars via a python script. 

## Synopsis

The script uses:

- Existing REST endpoints for [Confluence Server](https://developer.atlassian.com/server/confluence/confluence-server-rest-api/) and [Confluence Cloud](https://developer.atlassian.com/cloud/confluence/rest/)
- Two CSV mapping files, one each from Server and Cloud (See prerequisites below)

## Prerequisites

The script needs:

- Python 3.8 or later
- Preferably a Linux bash environment (not tested on Windows so if there are issues running it on Windows, please use a Linux host or VM instead)
- A comma-separated mapping file of Space Keys and Calendar IDs, generated from source Confluence Server
- A comma-separated mapping file of email addresses and Atlassian Account IDs, generated from target Confluence Cloud

(i) To turn off notifications that are sent to users for the creation of calendars, please let the support team know to temporarily disable Team Calendar notifications on the cloud site until the migration is completes.

For a successful run, please ensure the following for the user that runs the script:
- Script runner must be a site admin on the target Confluence cloud site
- Script runner must be an admin on the source Confluence server site
- On the ```Confluence Cloud Administration --> Team Calendars``` page, please ensure **Allow site administrators to manage calendars** is checked


## Running the migration

### Run the calendar migration
Here are the steps:

- Clone, or download, the script repository

- Generate the Space Keys <--> Calendar IDs mapping file using the following query (postgres-specific):

```sql
SELECT "SPACE_KEY", "ID", "NAME" FROM "AO_950DC3_TC_SUBCALS" 
WHERE length(coalesce("SPACE_KEY", '')) > 0
AND "STORE_KEY" NOT LIKE '%Subscrip%'
AND "PARENT_ID" IS NULL
ORDER BY 1,2;
```
(i) The above query works for PostgreSQL. It can be modified for other popular database vendors as follows:


1. **Oracle**:
```sql
SELECT SPACE_KEY, ID, NAME FROM AO_950DC3_TC_SUBCALS 
WHERE length(coalesce(SPACE_KEY, '')) > 0
AND STORE_KEY NOT LIKE '%Subscrip%'
AND PARENT_ID IS NULL
ORDER BY 1,2;
```

2. **SQL Server**
```sql
SELECT SPACE_KEY, ID, NAME FROM AO_950DC3_TC_SUBCALS 
WHERE len(coalesce(SPACE_KEY, '')) > 0
AND STORE_KEY NOT LIKE '%Subscrip%'
AND PARENT_ID IS NULL
ORDER BY 1,2;
```

3. **MySQL**
```sql
SELECT SPACE_KEY, ID, NAME FROM AO_950DC3_TC_SUBCALS 
WHERE length(coalesce(SPACE_KEY, '')) > 0
AND STORE_KEY NOT LIKE '%Subscrip%'
AND PARENT_ID IS NULL
ORDER BY 1,2;
```

- Save the output as CSV, say *space_calendars.csv*. If the values in the output file are enclosed in double-quotes, please run the following command to remove them:

```sed -i 's/\"//g' space_calendars.csv```

NOTE: If using `sed` on a MacOS terminal, please modify the command as below:

```sed -i '' 's/\"//g' space_calendars.csv```

  - Generate the email addresses <--> Atlassian Account IDs mapping file as follows:

    - Go to ```Confluence Cloud Administration --> User Management``` page
    - Click *Export users*
    - Choose 'All users on the site', 'Only active users', 'Product access', and click *Download file*
    - Run the following bash command on the exported file:
```bash
$ awk -F"," '{ printf("%s,%s\n", $3, $1) }' [exported file] > aaid_mapping.csv
```
- Install python modules
```bash
$ pip3 install -r requirements.txt
```
- Run the script as follows:
```bash
$ python3 migrateTeamCal.py -s <source confluence server URL> -t <target cloud site URL> -su <source admin user> -sp <source admin password> -tu <cloud site admin email> -tp <cloud site admin API key> --input-file space_calendars.csv --aaid aaid_mapping.csv
```
Notes:

  - **GOTCHA**: Source and target URLs must not have a trailing slash i.e. http://confluence:8090 and not http://confluence:8090/

  - By default, the script creates a logfile named as [cloud site]_full_migration_YYYY-MM-DD_HHMI.log. This can be overridden by --logfile parameter to the script

  - By default, the script processes the entire `space_calendars.csv` file. This can be overridden by providing `--space` parameter. **(i) This will migrate just that one space!**

  - For debugging, `--debug` option can be used.

  - To provide verbose debugging output when requested, `--verbose` option can be used.

- The script also creates two files for keeping track of calendars processed:
  - ```migrated_calendars_<DATE>.csv```: This file logs calendars that were successfully migrated
  - ```invalid_calendars_<DATE>.csv```: This file logs calendars that were invalid or could not be migrated

Full usage details of the script are:
```bash
python3 ./migrateTeamCal.py -h
usage: migrateTeamCal.py [-h] --source SOURCE --target TARGET --source-user SOURCE_USER --source-password SOURCE_PASSWORD --target-user TARGET_USER --target-password TARGET_PASSWORD
                         [--input-file INPUT_FILE] --aaid AAID [--default-invitee DEFAULT_INVITEE] [--default-creator DEFAULT_CREATOR] [--space SPACE] [--date-format DATE_FORMAT]
                         [--time-format TIME_FORMAT] [--stop-on-error] [--logfile LOGFILE] [--no-output] [--source-appid SOURCE_APPID] [--target-appid TARGET_APPID] [--retain] [--pagesize PAGESIZE]
                         [--startat STARTAT] [--confirm] [--debug] [--verbose] [--preflight] [--email-map EMAIL_MAP]

Migrate Confluence Team Calendars

options:
  -h, --help            show this help message and exit
  --source SOURCE, -s SOURCE
                        Source Confluence Server Base URL (example: http://localhost:8090)
  --target TARGET, -t TARGET
                        Target Confluence Cloud URL (example: https://my-cloud-site.atlassian.net)
  --source-user SOURCE_USER, -su SOURCE_USER
                        Username for source
  --source-password SOURCE_PASSWORD, -sp SOURCE_PASSWORD
                        Password for source
  --target-user TARGET_USER, -tu TARGET_USER
                        Username (email id) for target
  --target-password TARGET_PASSWORD, -tp TARGET_PASSWORD
                        API Key of the user for target
  --input-file INPUT_FILE, -i INPUT_FILE
                        Input file with space to calendar mapping
  --aaid AAID, -a AAID  A CSV formatted file containing mapping of each user's email ID with their respectie AID
  --default-invitee DEFAULT_INVITEE, -di DEFAULT_INVITEE
                        Default invitee email (for events where all existing invitees may be invalid)
  --default-creator DEFAULT_CREATOR, -dc DEFAULT_CREATOR
                        Default calendar creator email (for calendars where creator user is not found)
  --space SPACE, -k SPACE
                        Space key, if migrating a single space
  --date-format DATE_FORMAT, -df DATE_FORMAT
                        Target Cloud date format
  --time-format TIME_FORMAT, -tf TIME_FORMAT
                        Target Cloud time format
  --stop-on-error       Force the script to stop on error
  --logfile LOGFILE, -l LOGFILE
                        Logfile to write output to
  --no-output, -no      Turn off logging to screen/terminal (useful if running in background or via a cron job
  --source-appid SOURCE_APPID, -sa SOURCE_APPID
                        Jira Server Application ID
  --target-appid TARGET_APPID, -ta TARGET_APPID
                        Jira Cloud Application ID
  --retain, -r          Retain all migrated calendars in the migrator's view
  --pagesize PAGESIZE, -ps PAGESIZE
                        Number of calendars to migrate (see also: --startat)
  --startat STARTAT, -st STARTAT
                        Start number of calendars to migrate (see also: --pagesize)
  --confirm, -c         Confirm after migrating each calendar
  --debug, -d           Enable printing of debug messages
  --verbose, -v         Enable verbose messaging for developer reference
  --preflight, -p       Preflight checks mode
  --email-map EMAIL_MAP, -e EMAIL_MAP
                        Mapping of server emails to cloud emails
```

### Generate the cloud database update script
The calendars on the cloud site, after migration, are owned by the site admin (the user that runs the migration). To ensure correct ownership of each calendar, we provide a second script called `generateDBscript.py`.

This script takes the logfile of the migration script to produce DB update statements. These can then be shared with Migration Support team to run on the cloud DB.

Run this script as follows:

```bash
$ python3 generateDBscript.py <migration log file> > teamcal_db_updates.sql
```

Then, attach the `teamcal_db_updates.sql` to your MOVE ticket to notify the Migration Support team for processing.

## Known Issues and workarounds
1. Events of type "Jira Project Releases", "Jira Agile Sprints", and "Jira Issue Dates" are not be migrated. You may see a NullPointerException in the logs similar to the following:

```2021-11-14 12:35:03 [ERROR] [putEvent] Could not create event 'XXXXXXX' due to error: ['java.lang.NullPointerException
at com.atlassian.confluence.extra.calendar3.calendarstore.AbstractJiraSubCalendarDataStore.saveInternal(AbstractJiraSubCalendarDataStore.java:264)
at com.atlassian.confluence.extra.calendar3.calendarstore.AbstractCalendarDataStore.save(AbstractCalendarDataStore.java:270)
at com.atlassian.confluence.extra.calendar3.calendarstore.generic.JiraIssueDatesSubCalendarDataStore.save(JiraIssueDatesSubCalendarDataStore.java:73)
```

Workaround: Manually add these events in the target cloud team calendars. See [Add Jira Events to Team Calendars](https://support.atlassian.com/confluence-cloud/docs/add-jira-events/) for details.

2. Calendars that are subscribed but do not belong to the space (type: internal subscription) are not migrated. These may appear as following in the log file:

```2021-11-14 12:56:16 [ERROR] [putCalendar] Could not create calendar XXXXX on https://cloud.atlassian.net. Response is {'success': False, 'fieldErrors': [{'field': 'location', 'errorMessages': ['Field cannot be blank']}]}```

Workaround: Subscribe to these calendars again on target cloud site.

## Reporting issues and/or Suggestions

Sure, please go to [Issue Tracker](https://bitbucket.org/atlassianlabs/team-calendars/issues?status=new&status=open) and log an issue. 

Please also consider voting, and adding your comment to the open feature request for CCMA:

[Support the Migration of Team Calendar for Confluence](https://jira.atlassian.com/browse/MIG-124)
