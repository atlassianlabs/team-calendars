import csv
from datetime import *
from rich.console import Console
from rich.style import Style
import requests
from migrateTeamCal import print_info, print_debug, validateCloudUser, print_warning, print_verbose, print_error, outputFile, \
    show_version, console, init_rich, readCalendars, chunkit, CHUNKSIZE, getSomeCalendars, keyDict, nameDict, spaceList, notMigratedSpaces, getCloudSpaces, \
    creatorDict

def preflight_checks(source, target, source_user, source_password, target_user, target_password, input_file, aaid, space="", date_format="", time_format="", stop_on_error=False, logfile="", no_output=False, default_invitee="", default_creator="", source_appid=None, target_appid=None, retain=True, pagesize=-1, startat=-1, confirm=False, debug=False, verbose=False, preflight=True, email_map=None):
    global mapCal
    global mapUid
    global timeFormat
    global stopOnFirstError
    global outputFile
    global noOutput
    global defaultInvitee
    global keyDict
    global nameDict
    global spaceList
    global notMigratedSpaces
    init_rich()
    noOutput = no_output
    cloudName = target.split('.')[0].split('/')[2]
    asnow = datetime.now().strftime("%Y-%m-%d_%H%M")
    if space and len(space) > 0:
        logFile = f'{cloudName}_{space}_preflight_{asnow}.log'
    else:
        logFile = f'{cloudName}_full_preflight_{asnow}.log'
    if logfile:
        logFile = logfile
    if not outputFile:
        outputFile = open(logFile, "a")
    show_version()
    stopOnFirstError = stop_on_error
    defaultInvitee = default_invitee
    invalidFile = open(f'invalid_calendars_{asnow}.csv', 'w')
    migratedFile = open(f'verified_calendars_{asnow}.csv', 'w')
    invalidWriter = csv.writer(invalidFile)
    migratedWriter = csv.writer(migratedFile)
    if len(input_file) > 0:
        i = open(input_file)
        if space and len(space) > 0:
            ids = readCalendars(i, space)
        else:
            ids = readCalendars(i)
    else:
        if not startat or startat < 0:
            startat = 0
        if not pagesize or pagesize < 1:
            pagesize = 10000
        if space:
            ids = fakeReadCalendars(getAllCalendars(source, startat, pagesize, source_user, source_password, space))
        else:
            ids = fakeReadCalendars(getAllCalendars(source, startat, pagesize, source_user, source_password))
    aids = ids
    if startat and pagesize:
        if startat >= 0 and pagesize > 0:
            aids = ids[startat * pagesize: (startat + 1) * pagesize]
    eventCount = 0
    calendarCount = 0
    failedCalendarCount = 0
    restrictionCount = 0
    invalidUserCount = 0
    print_info("Pre-flight checks for migration of team calendars from {} (source) to {} (target) started. Output log: {}".format(source, target, logFile))
    if getCloudSpaces(target, target_user, target_password) == 0:
        return "No space found on target cloud site {}. Cannot continue.".format(target)
    if space and len(space) > 0:
        print_info("Migrating space '{}' only.".format(space))
        if space not in spaceList:
            return "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(space)
    for c in chunkit(aids, CHUNKSIZE):
        result = getSomeCalendars(source, c, source_user, source_password)
        if not result:
            print_verbose("One of more of the following calendar(s) could not be fetched from server: {}".format(c))
            for c1 in chunkit(c,1):
                result1 = getSomeCalendars(source, c1, source_user, source_password)
                if not result1:
                    calID = c1[0]
                    debug_rec = f'{keyDict[calID]},{calID}'
                    if nameDict and calID in nameDict:
                        debug_rec = debug_rec + f',{nameDict[calID]}'
                    print_debug(f"Details for calendar {debug_rec} could not be fetched from server.")
                    print_warning(f'Calendar {debug_rec} is invalid and will not be migrated.')
                    invalidWriter.writerow([debug_rec])
                    failedCalendarCount +=1
                else:
                    for cal in result1:
                        calID = cal['subCalendar']['id']
                        uid = cal['subCalendar']['creator']
                        if uid == 'creator':
                            continue
                        if not calID in keyDict:
                            print_warning("There is no space associated with calendar {}".format(cal['subCalendar']['name']))
                        cloudId = validateCloudUser(source, target, uid, source_user, source_password, target_user,
                                                    target_password,
                                                    aaid, email_map)
                        if not cloudId:
                            if calID in keyDict:
                                cal_rec = f'{keyDict[calID]},{calID}'
                            else:
                                cal_rec = f',{calID}'
                            if nameDict and calID in nameDict:
                                cal_rec = cal_rec + f',{nameDict[calID]}'
                            if creatorDict and calID in creatorDict:
                                cal_rec = cal_rec + f',{creatorDict[calID]}'
                            invalidWriter.writerow([cal_rec])
                            print_warning(f"Calendar {cal_rec} will not be migrated.")
                            invalidUserCount += 1
                        else:
                            if len(keyDict[calID]) == 0:
                                skey = keyDict[calID]
                            else:
                                skey = '~' + cloudId
                                keyDict[calID] = skey
                            if skey in notMigratedSpaces:
                                continue
                            if skey not in spaceList:
                                notMigratedSpaces.append(skey)
                                if '~' in skey:
                                    print_warning(
                                        "Personal space {} does not exist on cloud. Cannot migrate any calendars for this space/user.".format(
                                            skey))
                                else:
                                    print_warning(
                                        "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(
                                            skey))
                                continue
                            cal_rec = f'{keyDict[calID]},{calID}'
                            if nameDict and calID in nameDict:
                                cal_rec = cal_rec + f',{nameDict[calID]}'
                            if creatorDict and calID in creatorDict:
                                cal_rec = cal_rec + f',{creatorDict[calID]}'
                            migratedWriter.writerow([cal_rec])
                            print_info(f"Calendar {cal_rec} will be migrated as user {cloudId} on {target}.")
                        '''
                        skey = keyDict[calID]
                        if skey and len(skey) > 0:
                            if skey in notMigratedSpaces:
                                continue
                            if skey not in spaceList:
                                notMigratedSpaces.append(skey)
                                print_warning("Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(skey))
                                continue
                        cal_rec = f'{keyDict[calID]},{calID}'
                        if nameDict and calID in nameDict:
                            cal_rec = cal_rec + f',{nameDict[calID]}'
                        print_info(f"Calendar {cal_rec} is valid.")
                        uid = cal['subCalendar']['creator']
                        if uid != 'creator':
                        '''
        else:
            for cal in result:
                calID = cal['subCalendar']['id']
                uid = cal['subCalendar']['creator']
                if uid == 'creator':
                    continue
                if not calID in keyDict:
                    print_warning(
                        "There is no space associated with calendar {}.".format(cal['subCalendar']['name']))
                cloudId = validateCloudUser(source, target, uid, source_user, source_password, target_user,
                                            target_password,
                                            aaid, email_map)
                if not cloudId:
                    if calID in keyDict:
                        cal_rec = f'{keyDict[calID]},{calID}'
                    else:
                        cal_rec = f',{calID}'
                    if nameDict and calID in nameDict:
                        cal_rec = cal_rec + f',{nameDict[calID]}'
                    if creatorDict and calID in creatorDict:
                        cal_rec = cal_rec + f',{creatorDict[calID]}'
                    invalidWriter.writerow([cal_rec])
                    print_warning(f"Calendar {cal_rec} will not be migrated.")
                    invalidUserCount += 1
                else:
                    if keyDict[calID]:
                        skey = keyDict[calID]
                    else:
                        skey = '~' + cloudId
                        keyDict[calID] = skey
                    if skey in notMigratedSpaces:
                        continue
                    if skey not in spaceList:
                        notMigratedSpaces.append(skey)
                        if '~' in skey:
                            print_warning(
                                "Personal space {} does not exist on cloud. Cannot migrate any calendars for this space/user.".format(skey))
                        else:
                            print_warning(
                                "Space {} does not exist on cloud. Cannot migrate any calendars for this space.".format(skey))
                        continue
                    cal_rec = f'{keyDict[calID]},{calID}'
                    if nameDict and calID in nameDict:
                        cal_rec = cal_rec + f',{nameDict[calID]}'
                    if creatorDict and calID in creatorDict:
                        cal_rec = cal_rec + f',{creatorDict[calID]}'
                    migratedWriter.writerow([cal_rec])
                    print_info(f"Calendar {cal_rec} will be migrated as user {cloudId} on {target}.")

    #            for cal in result:
#                calID = cal['subCalendar']['id']
#                if creatorDict[calID]:
#                    uid = creatorDict[calID]
#                if uid != 'creator':
#                    cloudId = validateCloudUser(source, target, uid, source_user, source_password, target_user, target_password, aaid, email_map)
#                    if not cloudId:
#                        invalidWriter.writerow([cal_rec])
#                        print_warning(f"Calendar {cal_rec} will not be migrated (invalid owner/creator).")
#                        invalidUserCount += 1
#                    else:
#                        migratedWriter.writerow([cal_rec])
#                        print_info(f"Calendar {cal_rec} will be migrated as {cloudId} on {target}.")
#                # uid = cal['subCalendar']['creator']

    if invalidUserCount == 0:
        print_info("All calendar creators/owners, from {}, validated on {}".format(source, target))
    if space and len(space) > 0:
        return f"Preflight checks for migration of team calendars from {source} to {target} for space '{space}' finished (migrated {eventCount} events and {restrictionCount} restrictions in {calendarCount} calendars)."
    else:
        return f'Preflight checks for migration of team calendars from {source} to {target} finished (migrated {eventCount} events and {restrictionCount} restrictions in {calendarCount} calendars).'
